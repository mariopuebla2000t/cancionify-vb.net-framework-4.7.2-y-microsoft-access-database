﻿Public Class Album

    Private _idALB As String
    Private _nombreALB As String
    Private _fechaALB As Date
    Private _idArtistaALB As Integer
    Private _portadaALB As String
    Private _AlbDAO As AlbumDAO

    Sub New(ByVal idALB As Integer, ByVal nombreALB As String, ByVal fechaALB As Date, ByVal idArtistaALB As Integer, ByVal portadaALB As String)
        Me._idALB = idALB
        Me._nombreALB = nombreALB
        Me._fechaALB = fechaALB
        Me._idArtistaALB = idArtistaALB
        Me._portadaALB = portadaALB

        Me._AlbDAO = New AlbumDAO
    End Sub

    Sub New(ByVal idALB As Integer)
        Me._idALB = idALB

        Me._AlbDAO = New AlbumDAO
    End Sub

    Sub New()
        Me._AlbDAO = New AlbumDAO
    End Sub
    Public Property idALB As Integer
        Get
            Return Me._idALB
        End Get
        Set(value As Integer)
            Me._idALB = value
        End Set
    End Property

    Public Property nombreALB As String
        Get
            Return Me._nombreALB
        End Get
        Set(value As String)
            Me._nombreALB = value
        End Set
    End Property

    Public Property fechaALB As Date
        Get
            Return Me._fechaALB
        End Get
        Set(value As Date)
            Me._fechaALB = value
        End Set
    End Property

    Public Property idArtistaALB As Integer
        Get
            Return Me._idArtistaALB
        End Get
        Set(value As Integer)
            Me._idArtistaALB = value
        End Set
    End Property

    Public Property portadaALB As String
        Get
            Return Me._portadaALB
        End Get
        Set(value As String)
            Me._portadaALB = value
        End Set
    End Property

    Public Property AlbDAO As AlbumDAO
        Get
            Return Me._AlbDAO
        End Get
        Set(value As AlbumDAO)

        End Set
    End Property

    Public Function insertarAlbum() As Integer
        Return Me.AlbDAO.insertar(Me)
    End Function
    Public Function eliminarAlbum() As Integer
        Return Me.AlbDAO.eliminar(Me)
    End Function
    Public Function modificarAlbum() As Integer
        Return Me.AlbDAO.modificar(Me)
    End Function
    Public Sub leerAlbum()
        Me.AlbDAO.leer(Me)
    End Sub
    Public Sub leerTodoAlbum(ruta As String)
        Me.AlbDAO.leerTodo(ruta)
    End Sub

End Class
