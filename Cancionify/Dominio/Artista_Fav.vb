﻿Public Class Artista_Fav

    Private _usuarioARTFav As String
    Private _artistaFav As Integer
    Private _fechaFav As Date
    Private _ArFavDAO As ArtistaDAO

    Sub New(ByVal usuarioARTFav As String, ByVal artistaFav As Integer, ByVal fechaFav As Date)

        Me._usuarioARTFav = usuarioARTFav
        Me._artistaFav = artistaFav
        Me._fechaFav = _fechaFav

        Me._ArFavDAO = New ArtistaDAO

    End Sub

    Sub New()
        Me._ArFavDAO = New ArtistaDAO
    End Sub
    Public Property usuarioARTFav As Integer
        Get
            Return Me._usuarioARTFav
        End Get
        Set(value As Integer)
            Me._usuarioARTFav = value
        End Set
    End Property

    Public Property artistaFav As Integer
        Get
            Return Me._artistaFav
        End Get
        Set(value As Integer)
            Me._artistaFav = value
        End Set
    End Property

    Public Property fechaFav As Date
        Get
            Return Me._fechaFav
        End Get
        Set(value As Date)
            Me._fechaFav = value
        End Set
    End Property
    Public Property ArFavDAO As ArtistaDAO
        Get
            Return Me._ArFavDAO
        End Get
        Set(value As ArtistaDAO)

        End Set
    End Property
End Class
