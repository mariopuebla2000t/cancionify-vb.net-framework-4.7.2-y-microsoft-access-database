﻿Public Class Usuario

    Private _emailUS As String
    Private _nombreUS As String
    Private _apellidoUS As String
    Private _fechaNacimientoUS As Date
    Private _artistaFavoritosUS As Collection
    Private _reproduccionesUS As Collection
    Private _UsDAO As UsuarioDAO

    Sub New(ByVal emailUS As String, ByVal nombreUS As String, ByVal apellidoUS As String, ByVal fechaNacimientoUS As Date)
        Me._emailUS = emailUS
        Me._nombreUS = nombreUS
        Me._apellidoUS = apellidoUS
        Me._fechaNacimientoUS = fechaNacimientoUS

        Me._UsDAO = New UsuarioDAO
        Me._reproduccionesUS = New Collection
        Me._artistaFavoritosUS = New Collection

    End Sub

    Sub New(ByVal emailUS As String)
        Me._emailUS = emailUS

        Me._UsDAO = New UsuarioDAO
        Me._reproduccionesUS = New Collection
        Me._artistaFavoritosUS = New Collection

    End Sub

    Sub New()
        Me._UsDAO = New UsuarioDAO
        Me._reproduccionesUS = New Collection
        Me._artistaFavoritosUS = New Collection
    End Sub
    Public Property emailUS As String
        Get
            Return Me._emailUS
        End Get
        Set(value As String)
            Me._emailUS = value
        End Set
    End Property

    Public Property nombreUS As String
        Get
            Return Me._nombreUS
        End Get
        Set(value As String)
            Me._nombreUS = value
        End Set
    End Property

    Public Property apellidoUS As String
        Get
            Return Me._apellidoUS
        End Get
        Set(value As String)
            Me._apellidoUS = value
        End Set
    End Property

    Public Property fechaNacimientoUS As Date
        Get
            Return Me._fechaNacimientoUS
        End Get
        Set(value As Date)
            Me._fechaNacimientoUS = value
        End Set
    End Property

    Public Property reproduccionesUS As Collection
        Get
            Return Me._reproduccionesUS
        End Get
        Set(value As Collection)

        End Set
    End Property

    Public Property artistaFavoritosUS As Collection
        Get
            Return Me._artistaFavoritosUS
        End Get
        Set(value As Collection)

        End Set
    End Property

    Public Property UsDAO As UsuarioDAO
        Get
            Return Me._UsDAO
        End Get
        Set(value As UsuarioDAO)

        End Set
    End Property

    Public Function insertarUsuario() As Integer
        Return Me.UsDAO.insertar(Me)
    End Function
    Public Function eliminarUsuario() As Integer
        Return Me.UsDAO.eliminar(Me)
    End Function
    Public Function modificarUsuario() As Integer
        Return Me.UsDAO.modificar(Me)
    End Function
    Public Sub leerUsuario()
        Me.UsDAO.leer(Me)
    End Sub
    Public Sub leerTodoUsuario(ruta As String)
        Me.UsDAO.leerTodo(ruta)
    End Sub

    Public Sub artistasMEsc(fInicio As Date, fFin As Date)
        Me.UsDAO.artistasEscuchados(fInicio, fFin, Me)
    End Sub

    Public Sub ususMasT()
        Me.UsDAO.usuarioMasT()
    End Sub

    Public Sub tReproArtFavUsu()
        Me.UsDAO.tReproArtFav(Me)
    End Sub

End Class
