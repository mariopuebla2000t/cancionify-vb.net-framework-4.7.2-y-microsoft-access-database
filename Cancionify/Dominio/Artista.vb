﻿Public Class Artista

    Private _idArtistaART As Integer
    Private _nombreART As String
    Private _paisART As String
    Private _imagenART As String
    Private _ArDAO As ArtistaDAO

    Sub New(ByVal idArtistaART As Integer, ByVal nombreART As String, ByVal paisART As String, ByVal imagenART As String)
        Me._idArtistaART = idArtistaART
        Me._nombreART = nombreART
        Me._paisART = paisART
        Me._imagenART = imagenART

        Me._ArDAO = New ArtistaDAO
    End Sub

    Sub New(ByVal idArtistaART As Integer)
        Me._idArtistaART = idArtistaART
        Me._ArDAO = New ArtistaDAO
    End Sub

    Sub New()
        Me._ArDAO = New ArtistaDAO
    End Sub

    Public Property idArtistaART As Integer
        Get
            Return Me._idArtistaART
        End Get
        Set(value As Integer)
            Me._idArtistaART = value
        End Set
    End Property

    Public Property nombreART As String
        Get
            Return Me._nombreART
        End Get
        Set(value As String)
            Me._nombreART = value
        End Set
    End Property

    Public Property paisART As String
        Get
            Return Me._paisART
        End Get
        Set(value As String)
            Me._paisART = value
        End Set
    End Property

    Public Property imagenART As String
        Get
            Return Me._imagenART
        End Get
        Set(value As String)
            Me._imagenART = value
        End Set
    End Property

    Public Property ArDAO As ArtistaDAO
        Get
            Return Me._ArDAO
        End Get
        Set(value As ArtistaDAO)

        End Set
    End Property


    Public Function insertarArtista() As Integer
        Return Me.ArDAO.insertar(Me)
    End Function
    Public Function eliminarArtista() As Integer
        Return Me.ArDAO.eliminar(Me)
    End Function
    Public Function modificarArtista() As Integer
        Return Me.ArDAO.modificar(Me)
    End Function
    Public Sub leerArtista()
        Me.ArDAO.leer(Me)
    End Sub
    Public Sub leerTodoArtista(ruta As String)
        Me.ArDAO.leerTodo(ruta)
    End Sub

    Public Function favorito(ByRef U As Usuario) As Boolean
        Return Me.ArDAO.leerFavs(Me, U)
    End Function

    Public Sub leerArtFav(ByRef u As Usuario)
        Me.ArDAO.leerFavs(Me, u)
    End Sub

    Public Sub insertarARTFav(ByRef u As Usuario)
        Me.ArDAO.insertarFav(Me, u)
    End Sub

    Public Sub eliminarARTFav(ByRef u As Usuario)
        Me.ArDAO.eliminarFav(Me, u)
    End Sub

    Public Sub eliminarFavoritosDelUsuario(ByRef u As Usuario)
        Me.ArDAO.eliminarFavsDelUsuario(u)
    End Sub
    Public Sub eliminarArtistaDeFavoritos()
        Me.ArDAO.eliminarArtistaDeFavs(Me)
    End Sub
End Class
