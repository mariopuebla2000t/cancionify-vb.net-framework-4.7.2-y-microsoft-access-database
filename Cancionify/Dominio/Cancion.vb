﻿Public Class Cancion


    Private _idCANC As Integer
    Private _nombreCANC As String
    Private _duracionCANC As Integer
    Private _albumCANC As Integer
    Private _CanDAO As CancionDAO

    Sub New(ByVal idCANC As Integer, ByVal nombreCANC As String, ByVal albumCANC As Integer, ByVal duracionCANC As Integer)
        Me._idCANC = idCANC
        Me._nombreCANC = nombreCANC
        Me._albumCANC = albumCANC
        Me._duracionCANC = duracionCANC

        Me._CanDAO = New CancionDAO
    End Sub

    Sub New(ByVal idCANC As Integer, ByVal nombreCANC As String, ByVal albumCANC As Integer)
        Me._idCANC = idCANC
        Me._nombreCANC = nombreCANC
        Me._albumCANC = albumCANC

        Me._CanDAO = New CancionDAO
    End Sub

    Sub New(ByVal idCANC As Integer)
        Me._idCANC = idCANC
        Me._CanDAO = New CancionDAO
    End Sub

    Sub New()
        Me._CanDAO = New CancionDAO
    End Sub

    Public Property idCANC As Integer
        Get
            Return Me._idCANC
        End Get
        Set(value As Integer)
            Me._idCANC = value
        End Set
    End Property

    Public Property nombreCANC As String
        Get
            Return Me._nombreCANC
        End Get
        Set(value As String)
            Me._nombreCANC = value
        End Set
    End Property

    Public Property duracionCANC As Integer
        Get
            Return Me._duracionCANC
        End Get
        Set(value As Integer)
            Me._duracionCANC = value
        End Set
    End Property

    Public Property albumCANC As Integer
        Get
            Return Me._albumCANC
        End Get
        Set(value As Integer)
            Me._albumCANC = value
        End Set
    End Property

    Public Property CanDAO As CancionDAO
        Get
            Return Me._CanDAO
        End Get
        Set(value As CancionDAO)

        End Set
    End Property

    Public Function insertarCancion() As Integer
        Return Me.CanDAO.insertar(Me)
    End Function
    Public Function eliminarCancion() As Integer
        Return Me.CanDAO.eliminar(Me)
    End Function
    Public Function modificarCancion() As Integer
        Return Me.CanDAO.modificar(Me)
    End Function
    Public Sub leerCancion()
        Me.CanDAO.leer(Me)
    End Sub
    Public Sub leerTodoCancion(ruta As String)
        Me.CanDAO.leerTodo(ruta)
    End Sub

    Public Sub cancionesOrdenadas()
        Me.CanDAO.cancionesXRepro()
    End Sub
End Class
