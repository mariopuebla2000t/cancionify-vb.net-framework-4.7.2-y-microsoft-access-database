﻿Public Class Reproduccion

    Private _idReproduccion As Integer
    Private _usuarioReproducion As Usuario
    Private _cancionReproducion As Cancion
    Private _fechaReproduccion As Date

    Private _ReDAO As ReproduccionDAO

    Public Sub New(idReproduccion As Integer, usuarioRepro As String, cancionRepro As Integer, fechaReproduccion As Date)
        Dim u As Usuario = New Usuario(usuarioRepro)
        u.leerUsuario()
        Dim c As Cancion = New Cancion(cancionRepro)
        c.leerCancion()

        Me._idReproduccion = idReproduccion
        Me._usuarioReproducion = u
        Me._cancionReproducion = c
        Me._fechaReproduccion = fechaReproduccion
        Me._ReDAO = New ReproduccionDAO
    End Sub

    'Public Sub New(usuarioReproduccion As Usuario, cancionReproducion As Cancion, fechaReproduccion As Date)
    '    Me._usuarioReproducion = usuarioReproduccion
    '    Me._cancionReproducion = cancionReproducion
    '    Me._fechaReproduccion = fechaReproduccion
    '    Me._ReDAO = New ReproduccionDAO
    'End Sub

    Public Sub New(ByVal idReproduccion As Integer)
        Me._idReproduccion = idReproduccion
        Me._ReDAO = New ReproduccionDAO
    End Sub

    Public Sub New()
        Me._ReDAO = New ReproduccionDAO
    End Sub

    Public Property IdReproduccion As Integer
        Get
            Return _idReproduccion
        End Get
        Set(value As Integer)
            _idReproduccion = value
        End Set
    End Property

    Public Property usuarioRepro As Usuario
        Get
            Return _usuarioReproducion
        End Get
        Set(value As Usuario)
            _usuarioReproducion = value
        End Set
    End Property

    Public Property cancionRepro As Cancion
        Get
            Return _cancionReproducion
        End Get
        Set(value As Cancion)
            _cancionReproducion = value
        End Set
    End Property

    Public Property fechaRepro As Date
        Get
            Return _fechaReproduccion
        End Get
        Set(value As Date)
            _fechaReproduccion = value
        End Set
    End Property
    Public Property ReDAO As ReproduccionDAO
        Get
            Return Me._ReDAO
        End Get
        Set(value As ReproduccionDAO)

        End Set
    End Property
    Public Sub leerReproduccion()
        Me.ReDAO.leer(Me)
    End Sub

    Public Sub leerTodoReproduccion(u As Usuario)
        Me.ReDAO.leerTodo(u)
    End Sub

    Public Function insertarReproduccion() As Integer
        Return ReDAO.insertar(Me)
    End Function

    Public Function eliminarReproduccion() As Integer
        Return ReDAO.eliminar(Me)
    End Function

End Class
