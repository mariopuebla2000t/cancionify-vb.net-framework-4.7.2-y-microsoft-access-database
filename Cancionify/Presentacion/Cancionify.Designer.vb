﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CANCIONIFY
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CANCIONIFY))
        Me.gbCancionfy = New System.Windows.Forms.GroupBox()
        Me.lblUsuarioSeleccionadoInfo = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tabPCancionify = New System.Windows.Forms.TabControl()
        Me.tabPUsuarios = New System.Windows.Forms.TabPage()
        Me.btnMostrarUsMasT = New System.Windows.Forms.Button()
        Me.lblUsMasT = New System.Windows.Forms.Label()
        Me.lstUsMasT = New System.Windows.Forms.ListBox()
        Me.lblHasta = New System.Windows.Forms.Label()
        Me.lblDesde = New System.Windows.Forms.Label()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.btnARTMasEscuchadoUS = New System.Windows.Forms.Button()
        Me.lblArtMEscuchadosUS = New System.Windows.Forms.Label()
        Me.lstArtMEscuchadosUS = New System.Windows.Forms.ListBox()
        Me.lstUsuarios = New System.Windows.Forms.ListBox()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txtApellidos = New System.Windows.Forms.TextBox()
        Me.txtFechaNac = New System.Windows.Forms.TextBox()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.btnEliminarUsuario = New System.Windows.Forms.Button()
        Me.btnLimpiarUsuario = New System.Windows.Forms.Button()
        Me.lblFechaNac = New System.Windows.Forms.Label()
        Me.btnModificarUsuario = New System.Windows.Forms.Button()
        Me.lblApellidos = New System.Windows.Forms.Label()
        Me.btnAnadirUsuario = New System.Windows.Forms.Button()
        Me.tabPArtistas = New System.Windows.Forms.TabPage()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnTReproArtFav = New System.Windows.Forms.Button()
        Me.lstTReproArtFav = New System.Windows.Forms.ListBox()
        Me.lblEsFavorito = New System.Windows.Forms.Label()
        Me.btnQuitarFav = New System.Windows.Forms.Button()
        Me.btnFavorito = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lstAlbumesART = New System.Windows.Forms.ListBox()
        Me.pbArtista = New System.Windows.Forms.PictureBox()
        Me.lstArtistasFav = New System.Windows.Forms.ListBox()
        Me.btnEliminarArtista = New System.Windows.Forms.Button()
        Me.btnLimpiarArtista = New System.Windows.Forms.Button()
        Me.btnModificarArtista = New System.Windows.Forms.Button()
        Me.btnAnadirArtista = New System.Windows.Forms.Button()
        Me.lblImagenArt = New System.Windows.Forms.Label()
        Me.lblPaisArt = New System.Windows.Forms.Label()
        Me.lblNombreArt = New System.Windows.Forms.Label()
        Me.lblIdArtista = New System.Windows.Forms.Label()
        Me.txtNombreArt = New System.Windows.Forms.TextBox()
        Me.txtPaisArt = New System.Windows.Forms.TextBox()
        Me.txtImagenArt = New System.Windows.Forms.TextBox()
        Me.txtIdArt = New System.Windows.Forms.TextBox()
        Me.lstArtistas = New System.Windows.Forms.ListBox()
        Me.tabPCanciones = New System.Windows.Forms.TabPage()
        Me.lblReprosUS = New System.Windows.Forms.Label()
        Me.lstReproducciones = New System.Windows.Forms.ListBox()
        Me.txtUsuarioReproduccion = New System.Windows.Forms.TextBox()
        Me.txtFechaReproduccion = New System.Windows.Forms.TextBox()
        Me.txtCancionReproduccion = New System.Windows.Forms.TextBox()
        Me.txtIdReproduccion = New System.Windows.Forms.TextBox()
        Me.txtDuracionCanc = New System.Windows.Forms.TextBox()
        Me.txtAlbumCanc = New System.Windows.Forms.TextBox()
        Me.txtNombreCanc = New System.Windows.Forms.TextBox()
        Me.txtIdCanc = New System.Windows.Forms.TextBox()
        Me.lblUsuarioReproduccion = New System.Windows.Forms.Label()
        Me.btnLimpiarR = New System.Windows.Forms.Button()
        Me.btnAnadirR = New System.Windows.Forms.Button()
        Me.lblFechaReproduccion = New System.Windows.Forms.Label()
        Me.lblCancionReproduccion = New System.Windows.Forms.Label()
        Me.lblIdReproduccion = New System.Windows.Forms.Label()
        Me.lstCanOrd = New System.Windows.Forms.ListBox()
        Me.lblOrdenarCAN = New System.Windows.Forms.Label()
        Me.btnOrdenarCAN = New System.Windows.Forms.Button()
        Me.lblDuracionCanc = New System.Windows.Forms.Label()
        Me.lblAlbumCanc = New System.Windows.Forms.Label()
        Me.lblNombreCanc = New System.Windows.Forms.Label()
        Me.lblIdCanc = New System.Windows.Forms.Label()
        Me.btnEliminarCancion = New System.Windows.Forms.Button()
        Me.btnLimpiarCancion = New System.Windows.Forms.Button()
        Me.btnModificarCancion = New System.Windows.Forms.Button()
        Me.btnAnadirCancion = New System.Windows.Forms.Button()
        Me.lstCanciones = New System.Windows.Forms.ListBox()
        Me.tabPAlbumes = New System.Windows.Forms.TabPage()
        Me.lstCancionesAlbum = New System.Windows.Forms.ListBox()
        Me.lblCancionesAlbum = New System.Windows.Forms.Label()
        Me.pbAlbumes = New System.Windows.Forms.PictureBox()
        Me.lblDuracionAlbum = New System.Windows.Forms.Label()
        Me.txtDuracionAlbum = New System.Windows.Forms.TextBox()
        Me.txtPortadaAlbum = New System.Windows.Forms.TextBox()
        Me.txtIdAlbum = New System.Windows.Forms.TextBox()
        Me.txtNombreAlbum = New System.Windows.Forms.TextBox()
        Me.txtFechaAlbum = New System.Windows.Forms.TextBox()
        Me.txtIdArtistaAlb = New System.Windows.Forms.TextBox()
        Me.lblImgAlbum = New System.Windows.Forms.Label()
        Me.lstAlbumes = New System.Windows.Forms.ListBox()
        Me.lblIdAlbum = New System.Windows.Forms.Label()
        Me.lblNombreAlbum = New System.Windows.Forms.Label()
        Me.btnEliminarAlbum = New System.Windows.Forms.Button()
        Me.btnLimpiarAlbum = New System.Windows.Forms.Button()
        Me.lblIdArtistaAlb = New System.Windows.Forms.Label()
        Me.btnModificarAlbum = New System.Windows.Forms.Button()
        Me.lblFechaAlbum = New System.Windows.Forms.Label()
        Me.btnAnadirAlbum = New System.Windows.Forms.Button()
        Me.btnRutaBD = New System.Windows.Forms.Button()
        Me.btnConectar = New System.Windows.Forms.Button()
        Me.txtRuta = New System.Windows.Forms.TextBox()
        Me.lblConectar = New System.Windows.Forms.Label()
        Me.ofdRuta = New System.Windows.Forms.OpenFileDialog()
        Me.lblReproduciendo = New System.Windows.Forms.Label()
        Me.gbCancionfy.SuspendLayout()
        Me.tabPCancionify.SuspendLayout()
        Me.tabPUsuarios.SuspendLayout()
        Me.tabPArtistas.SuspendLayout()
        CType(Me.pbArtista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabPCanciones.SuspendLayout()
        Me.tabPAlbumes.SuspendLayout()
        CType(Me.pbAlbumes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbCancionfy
        '
        Me.gbCancionfy.Controls.Add(Me.lblUsuarioSeleccionadoInfo)
        Me.gbCancionfy.Controls.Add(Me.Label3)
        Me.gbCancionfy.Controls.Add(Me.tabPCancionify)
        Me.gbCancionfy.Enabled = False
        Me.gbCancionfy.Location = New System.Drawing.Point(12, 12)
        Me.gbCancionfy.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.gbCancionfy.Name = "gbCancionfy"
        Me.gbCancionfy.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.gbCancionfy.Size = New System.Drawing.Size(1574, 383)
        Me.gbCancionfy.TabIndex = 0
        Me.gbCancionfy.TabStop = False
        Me.gbCancionfy.Text = "CANCIONIFY"
        '
        'lblUsuarioSeleccionadoInfo
        '
        Me.lblUsuarioSeleccionadoInfo.AutoSize = True
        Me.lblUsuarioSeleccionadoInfo.Location = New System.Drawing.Point(866, 29)
        Me.lblUsuarioSeleccionadoInfo.Name = "lblUsuarioSeleccionadoInfo"
        Me.lblUsuarioSeleccionadoInfo.Size = New System.Drawing.Size(24, 20)
        Me.lblUsuarioSeleccionadoInfo.TabIndex = 17
        Me.lblUsuarioSeleccionadoInfo.Text = "---"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(699, 29)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(161, 20)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Usuario selecionado: "
        '
        'tabPCancionify
        '
        Me.tabPCancionify.Controls.Add(Me.tabPUsuarios)
        Me.tabPCancionify.Controls.Add(Me.tabPArtistas)
        Me.tabPCancionify.Controls.Add(Me.tabPCanciones)
        Me.tabPCancionify.Controls.Add(Me.tabPAlbumes)
        Me.tabPCancionify.Location = New System.Drawing.Point(6, 35)
        Me.tabPCancionify.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tabPCancionify.Name = "tabPCancionify"
        Me.tabPCancionify.SelectedIndex = 0
        Me.tabPCancionify.Size = New System.Drawing.Size(1568, 348)
        Me.tabPCancionify.TabIndex = 15
        '
        'tabPUsuarios
        '
        Me.tabPUsuarios.Controls.Add(Me.btnMostrarUsMasT)
        Me.tabPUsuarios.Controls.Add(Me.lblUsMasT)
        Me.tabPUsuarios.Controls.Add(Me.lstUsMasT)
        Me.tabPUsuarios.Controls.Add(Me.lblHasta)
        Me.tabPUsuarios.Controls.Add(Me.lblDesde)
        Me.tabPUsuarios.Controls.Add(Me.dtpHasta)
        Me.tabPUsuarios.Controls.Add(Me.dtpDesde)
        Me.tabPUsuarios.Controls.Add(Me.btnARTMasEscuchadoUS)
        Me.tabPUsuarios.Controls.Add(Me.lblArtMEscuchadosUS)
        Me.tabPUsuarios.Controls.Add(Me.lstArtMEscuchadosUS)
        Me.tabPUsuarios.Controls.Add(Me.lstUsuarios)
        Me.tabPUsuarios.Controls.Add(Me.txtEmail)
        Me.tabPUsuarios.Controls.Add(Me.txtNombre)
        Me.tabPUsuarios.Controls.Add(Me.txtApellidos)
        Me.tabPUsuarios.Controls.Add(Me.txtFechaNac)
        Me.tabPUsuarios.Controls.Add(Me.lblEmail)
        Me.tabPUsuarios.Controls.Add(Me.lblNombre)
        Me.tabPUsuarios.Controls.Add(Me.btnEliminarUsuario)
        Me.tabPUsuarios.Controls.Add(Me.btnLimpiarUsuario)
        Me.tabPUsuarios.Controls.Add(Me.lblFechaNac)
        Me.tabPUsuarios.Controls.Add(Me.btnModificarUsuario)
        Me.tabPUsuarios.Controls.Add(Me.lblApellidos)
        Me.tabPUsuarios.Controls.Add(Me.btnAnadirUsuario)
        Me.tabPUsuarios.Location = New System.Drawing.Point(4, 29)
        Me.tabPUsuarios.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tabPUsuarios.Name = "tabPUsuarios"
        Me.tabPUsuarios.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tabPUsuarios.Size = New System.Drawing.Size(1560, 315)
        Me.tabPUsuarios.TabIndex = 0
        Me.tabPUsuarios.Text = "Usuarios"
        Me.tabPUsuarios.UseVisualStyleBackColor = True
        '
        'btnMostrarUsMasT
        '
        Me.btnMostrarUsMasT.Location = New System.Drawing.Point(791, 234)
        Me.btnMostrarUsMasT.Name = "btnMostrarUsMasT"
        Me.btnMostrarUsMasT.Size = New System.Drawing.Size(75, 36)
        Me.btnMostrarUsMasT.TabIndex = 25
        Me.btnMostrarUsMasT.Text = "Mostrar"
        Me.btnMostrarUsMasT.UseVisualStyleBackColor = True
        '
        'lblUsMasT
        '
        Me.lblUsMasT.AutoSize = True
        Me.lblUsMasT.Location = New System.Drawing.Point(746, 180)
        Me.lblUsMasT.Name = "lblUsMasT"
        Me.lblUsMasT.Size = New System.Drawing.Size(163, 20)
        Me.lblUsMasT.TabIndex = 24
        Me.lblUsMasT.Text = "Usuarios más activos:"
        '
        'lstUsMasT
        '
        Me.lstUsMasT.FormattingEnabled = True
        Me.lstUsMasT.ItemHeight = 20
        Me.lstUsMasT.Location = New System.Drawing.Point(1061, 186)
        Me.lstUsMasT.Name = "lstUsMasT"
        Me.lstUsMasT.Size = New System.Drawing.Size(335, 104)
        Me.lstUsMasT.TabIndex = 23
        '
        'lblHasta
        '
        Me.lblHasta.AutoSize = True
        Me.lblHasta.Location = New System.Drawing.Point(750, 86)
        Me.lblHasta.Name = "lblHasta"
        Me.lblHasta.Size = New System.Drawing.Size(52, 20)
        Me.lblHasta.TabIndex = 22
        Me.lblHasta.Text = "Hasta"
        '
        'lblDesde
        '
        Me.lblDesde.AutoSize = True
        Me.lblDesde.Location = New System.Drawing.Point(746, 40)
        Me.lblDesde.Name = "lblDesde"
        Me.lblDesde.Size = New System.Drawing.Size(56, 20)
        Me.lblDesde.TabIndex = 21
        Me.lblDesde.Text = "Desde"
        '
        'dtpHasta
        '
        Me.dtpHasta.Location = New System.Drawing.Point(819, 86)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(200, 26)
        Me.dtpHasta.TabIndex = 20
        '
        'dtpDesde
        '
        Me.dtpDesde.Location = New System.Drawing.Point(819, 40)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(200, 26)
        Me.dtpDesde.TabIndex = 19
        '
        'btnARTMasEscuchadoUS
        '
        Me.btnARTMasEscuchadoUS.Location = New System.Drawing.Point(944, 126)
        Me.btnARTMasEscuchadoUS.Name = "btnARTMasEscuchadoUS"
        Me.btnARTMasEscuchadoUS.Size = New System.Drawing.Size(75, 36)
        Me.btnARTMasEscuchadoUS.TabIndex = 18
        Me.btnARTMasEscuchadoUS.Text = "Mostrar"
        Me.btnARTMasEscuchadoUS.UseVisualStyleBackColor = True
        '
        'lblArtMEscuchadosUS
        '
        Me.lblArtMEscuchadosUS.AutoSize = True
        Me.lblArtMEscuchadosUS.Location = New System.Drawing.Point(746, 6)
        Me.lblArtMEscuchadosUS.Name = "lblArtMEscuchadosUS"
        Me.lblArtMEscuchadosUS.Size = New System.Drawing.Size(290, 20)
        Me.lblArtMEscuchadosUS.TabIndex = 17
        Me.lblArtMEscuchadosUS.Text = "Artistas más escuchados por el usuario:"
        '
        'lstArtMEscuchadosUS
        '
        Me.lstArtMEscuchadosUS.FormattingEnabled = True
        Me.lstArtMEscuchadosUS.ItemHeight = 20
        Me.lstArtMEscuchadosUS.Location = New System.Drawing.Point(1061, 6)
        Me.lstArtMEscuchadosUS.Name = "lstArtMEscuchadosUS"
        Me.lstArtMEscuchadosUS.Size = New System.Drawing.Size(320, 124)
        Me.lstArtMEscuchadosUS.TabIndex = 16
        '
        'lstUsuarios
        '
        Me.lstUsuarios.FormattingEnabled = True
        Me.lstUsuarios.ItemHeight = 20
        Me.lstUsuarios.Location = New System.Drawing.Point(4, 6)
        Me.lstUsuarios.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.lstUsuarios.Name = "lstUsuarios"
        Me.lstUsuarios.Size = New System.Drawing.Size(252, 284)
        Me.lstUsuarios.TabIndex = 15
        '
        'txtEmail
        '
        Me.txtEmail.Enabled = False
        Me.txtEmail.Location = New System.Drawing.Point(484, 22)
        Me.txtEmail.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(194, 26)
        Me.txtEmail.TabIndex = 1
        '
        'txtNombre
        '
        Me.txtNombre.Enabled = False
        Me.txtNombre.Location = New System.Drawing.Point(484, 55)
        Me.txtNombre.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(194, 26)
        Me.txtNombre.TabIndex = 4
        '
        'txtApellidos
        '
        Me.txtApellidos.Enabled = False
        Me.txtApellidos.Location = New System.Drawing.Point(484, 89)
        Me.txtApellidos.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtApellidos.Name = "txtApellidos"
        Me.txtApellidos.Size = New System.Drawing.Size(194, 26)
        Me.txtApellidos.TabIndex = 13
        '
        'txtFechaNac
        '
        Me.txtFechaNac.Enabled = False
        Me.txtFechaNac.Location = New System.Drawing.Point(484, 122)
        Me.txtFechaNac.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtFechaNac.Name = "txtFechaNac"
        Me.txtFechaNac.Size = New System.Drawing.Size(194, 26)
        Me.txtFechaNac.TabIndex = 14
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Location = New System.Drawing.Point(290, 25)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(48, 20)
        Me.lblEmail.TabIndex = 9
        Me.lblEmail.Text = "Email"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(290, 58)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(65, 20)
        Me.lblNombre.TabIndex = 10
        Me.lblNombre.Text = "Nombre"
        '
        'btnEliminarUsuario
        '
        Me.btnEliminarUsuario.Enabled = False
        Me.btnEliminarUsuario.Location = New System.Drawing.Point(522, 168)
        Me.btnEliminarUsuario.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnEliminarUsuario.Name = "btnEliminarUsuario"
        Me.btnEliminarUsuario.Size = New System.Drawing.Size(118, 42)
        Me.btnEliminarUsuario.TabIndex = 6
        Me.btnEliminarUsuario.Text = "Eliminar"
        Me.btnEliminarUsuario.UseVisualStyleBackColor = True
        '
        'btnLimpiarUsuario
        '
        Me.btnLimpiarUsuario.Enabled = False
        Me.btnLimpiarUsuario.Location = New System.Drawing.Point(522, 234)
        Me.btnLimpiarUsuario.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnLimpiarUsuario.Name = "btnLimpiarUsuario"
        Me.btnLimpiarUsuario.Size = New System.Drawing.Size(118, 42)
        Me.btnLimpiarUsuario.TabIndex = 8
        Me.btnLimpiarUsuario.Text = "Limpiar"
        Me.btnLimpiarUsuario.UseVisualStyleBackColor = True
        '
        'lblFechaNac
        '
        Me.lblFechaNac.AutoSize = True
        Me.lblFechaNac.Location = New System.Drawing.Point(290, 126)
        Me.lblFechaNac.Name = "lblFechaNac"
        Me.lblFechaNac.Size = New System.Drawing.Size(157, 20)
        Me.lblFechaNac.TabIndex = 12
        Me.lblFechaNac.Text = "Fecha de nacimiento"
        '
        'btnModificarUsuario
        '
        Me.btnModificarUsuario.Enabled = False
        Me.btnModificarUsuario.Location = New System.Drawing.Point(330, 234)
        Me.btnModificarUsuario.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnModificarUsuario.Name = "btnModificarUsuario"
        Me.btnModificarUsuario.Size = New System.Drawing.Size(118, 42)
        Me.btnModificarUsuario.TabIndex = 7
        Me.btnModificarUsuario.Text = "Modificar"
        Me.btnModificarUsuario.UseVisualStyleBackColor = True
        '
        'lblApellidos
        '
        Me.lblApellidos.AutoSize = True
        Me.lblApellidos.Location = New System.Drawing.Point(290, 92)
        Me.lblApellidos.Name = "lblApellidos"
        Me.lblApellidos.Size = New System.Drawing.Size(73, 20)
        Me.lblApellidos.TabIndex = 11
        Me.lblApellidos.Text = "Apellidos"
        '
        'btnAnadirUsuario
        '
        Me.btnAnadirUsuario.Enabled = False
        Me.btnAnadirUsuario.Location = New System.Drawing.Point(330, 168)
        Me.btnAnadirUsuario.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnAnadirUsuario.Name = "btnAnadirUsuario"
        Me.btnAnadirUsuario.Size = New System.Drawing.Size(118, 45)
        Me.btnAnadirUsuario.TabIndex = 5
        Me.btnAnadirUsuario.Text = "Añadir"
        Me.btnAnadirUsuario.UseVisualStyleBackColor = True
        '
        'tabPArtistas
        '
        Me.tabPArtistas.Controls.Add(Me.Label5)
        Me.tabPArtistas.Controls.Add(Me.btnTReproArtFav)
        Me.tabPArtistas.Controls.Add(Me.lstTReproArtFav)
        Me.tabPArtistas.Controls.Add(Me.lblEsFavorito)
        Me.tabPArtistas.Controls.Add(Me.btnQuitarFav)
        Me.tabPArtistas.Controls.Add(Me.btnFavorito)
        Me.tabPArtistas.Controls.Add(Me.Label2)
        Me.tabPArtistas.Controls.Add(Me.Label1)
        Me.tabPArtistas.Controls.Add(Me.lstAlbumesART)
        Me.tabPArtistas.Controls.Add(Me.pbArtista)
        Me.tabPArtistas.Controls.Add(Me.lstArtistasFav)
        Me.tabPArtistas.Controls.Add(Me.btnEliminarArtista)
        Me.tabPArtistas.Controls.Add(Me.btnLimpiarArtista)
        Me.tabPArtistas.Controls.Add(Me.btnModificarArtista)
        Me.tabPArtistas.Controls.Add(Me.btnAnadirArtista)
        Me.tabPArtistas.Controls.Add(Me.lblImagenArt)
        Me.tabPArtistas.Controls.Add(Me.lblPaisArt)
        Me.tabPArtistas.Controls.Add(Me.lblNombreArt)
        Me.tabPArtistas.Controls.Add(Me.lblIdArtista)
        Me.tabPArtistas.Controls.Add(Me.txtNombreArt)
        Me.tabPArtistas.Controls.Add(Me.txtPaisArt)
        Me.tabPArtistas.Controls.Add(Me.txtImagenArt)
        Me.tabPArtistas.Controls.Add(Me.txtIdArt)
        Me.tabPArtistas.Controls.Add(Me.lstArtistas)
        Me.tabPArtistas.Location = New System.Drawing.Point(4, 29)
        Me.tabPArtistas.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tabPArtistas.Name = "tabPArtistas"
        Me.tabPArtistas.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tabPArtistas.Size = New System.Drawing.Size(1560, 315)
        Me.tabPArtistas.TabIndex = 1
        Me.tabPArtistas.Text = "Artistas"
        Me.tabPArtistas.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(1324, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(183, 40)
        Me.Label5.TabIndex = 24
        Me.Label5.Text = "Tiempo de reproducción " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "de los artistas favoritos:"
        '
        'btnTReproArtFav
        '
        Me.btnTReproArtFav.Location = New System.Drawing.Point(1328, 63)
        Me.btnTReproArtFav.Name = "btnTReproArtFav"
        Me.btnTReproArtFav.Size = New System.Drawing.Size(126, 32)
        Me.btnTReproArtFav.TabIndex = 23
        Me.btnTReproArtFav.Text = "Mostrar tiempo"
        Me.btnTReproArtFav.UseVisualStyleBackColor = True
        '
        'lstTReproArtFav
        '
        Me.lstTReproArtFav.FormattingEnabled = True
        Me.lstTReproArtFav.ItemHeight = 20
        Me.lstTReproArtFav.Location = New System.Drawing.Point(1328, 112)
        Me.lstTReproArtFav.Name = "lstTReproArtFav"
        Me.lstTReproArtFav.Size = New System.Drawing.Size(198, 144)
        Me.lstTReproArtFav.TabIndex = 22
        '
        'lblEsFavorito
        '
        Me.lblEsFavorito.AutoSize = True
        Me.lblEsFavorito.Location = New System.Drawing.Point(1066, 22)
        Me.lblEsFavorito.Name = "lblEsFavorito"
        Me.lblEsFavorito.Size = New System.Drawing.Size(29, 20)
        Me.lblEsFavorito.TabIndex = 21
        Me.lblEsFavorito.Text = ". . ."
        '
        'btnQuitarFav
        '
        Me.btnQuitarFav.Location = New System.Drawing.Point(1053, 57)
        Me.btnQuitarFav.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnQuitarFav.Name = "btnQuitarFav"
        Me.btnQuitarFav.Size = New System.Drawing.Size(126, 32)
        Me.btnQuitarFav.TabIndex = 19
        Me.btnQuitarFav.Text = "Quitar Favorito"
        Me.btnQuitarFav.UseVisualStyleBackColor = True
        '
        'btnFavorito
        '
        Me.btnFavorito.Location = New System.Drawing.Point(933, 57)
        Me.btnFavorito.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnFavorito.Name = "btnFavorito"
        Me.btnFavorito.Size = New System.Drawing.Size(112, 32)
        Me.btnFavorito.TabIndex = 18
        Me.btnFavorito.Text = "Favorito"
        Me.btnFavorito.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(689, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(180, 20)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Albumes de este artista:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(929, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(131, 20)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Artistas favoritos:"
        '
        'lstAlbumesART
        '
        Me.lstAlbumesART.FormattingEnabled = True
        Me.lstAlbumesART.ItemHeight = 20
        Me.lstAlbumesART.Location = New System.Drawing.Point(693, 112)
        Me.lstAlbumesART.Name = "lstAlbumesART"
        Me.lstAlbumesART.Size = New System.Drawing.Size(219, 144)
        Me.lstAlbumesART.TabIndex = 15
        '
        'pbArtista
        '
        Me.pbArtista.Location = New System.Drawing.Point(494, 6)
        Me.pbArtista.Name = "pbArtista"
        Me.pbArtista.Size = New System.Drawing.Size(171, 188)
        Me.pbArtista.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbArtista.TabIndex = 14
        Me.pbArtista.TabStop = False
        Me.pbArtista.WaitOnLoad = True
        '
        'lstArtistasFav
        '
        Me.lstArtistasFav.FormattingEnabled = True
        Me.lstArtistasFav.ItemHeight = 20
        Me.lstArtistasFav.Location = New System.Drawing.Point(933, 112)
        Me.lstArtistasFav.Name = "lstArtistasFav"
        Me.lstArtistasFav.Size = New System.Drawing.Size(246, 144)
        Me.lstArtistasFav.TabIndex = 13
        '
        'btnEliminarArtista
        '
        Me.btnEliminarArtista.Enabled = False
        Me.btnEliminarArtista.Location = New System.Drawing.Point(368, 212)
        Me.btnEliminarArtista.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnEliminarArtista.Name = "btnEliminarArtista"
        Me.btnEliminarArtista.Size = New System.Drawing.Size(118, 34)
        Me.btnEliminarArtista.TabIndex = 10
        Me.btnEliminarArtista.Text = "Eliminar"
        Me.btnEliminarArtista.UseVisualStyleBackColor = True
        '
        'btnLimpiarArtista
        '
        Me.btnLimpiarArtista.Enabled = False
        Me.btnLimpiarArtista.Location = New System.Drawing.Point(233, 256)
        Me.btnLimpiarArtista.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnLimpiarArtista.Name = "btnLimpiarArtista"
        Me.btnLimpiarArtista.Size = New System.Drawing.Size(118, 34)
        Me.btnLimpiarArtista.TabIndex = 12
        Me.btnLimpiarArtista.Text = "Limpiar"
        Me.btnLimpiarArtista.UseVisualStyleBackColor = True
        '
        'btnModificarArtista
        '
        Me.btnModificarArtista.Enabled = False
        Me.btnModificarArtista.Location = New System.Drawing.Point(368, 258)
        Me.btnModificarArtista.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnModificarArtista.Name = "btnModificarArtista"
        Me.btnModificarArtista.Size = New System.Drawing.Size(118, 32)
        Me.btnModificarArtista.TabIndex = 11
        Me.btnModificarArtista.Text = "Modificar"
        Me.btnModificarArtista.UseVisualStyleBackColor = True
        '
        'btnAnadirArtista
        '
        Me.btnAnadirArtista.Enabled = False
        Me.btnAnadirArtista.Location = New System.Drawing.Point(233, 212)
        Me.btnAnadirArtista.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnAnadirArtista.Name = "btnAnadirArtista"
        Me.btnAnadirArtista.Size = New System.Drawing.Size(118, 34)
        Me.btnAnadirArtista.TabIndex = 9
        Me.btnAnadirArtista.Text = "Añadir"
        Me.btnAnadirArtista.UseVisualStyleBackColor = True
        '
        'lblImagenArt
        '
        Me.lblImagenArt.AutoSize = True
        Me.lblImagenArt.Location = New System.Drawing.Point(250, 158)
        Me.lblImagenArt.Name = "lblImagenArt"
        Me.lblImagenArt.Size = New System.Drawing.Size(63, 20)
        Me.lblImagenArt.TabIndex = 8
        Me.lblImagenArt.Text = "Imagen"
        '
        'lblPaisArt
        '
        Me.lblPaisArt.AutoSize = True
        Me.lblPaisArt.Location = New System.Drawing.Point(250, 112)
        Me.lblPaisArt.Name = "lblPaisArt"
        Me.lblPaisArt.Size = New System.Drawing.Size(39, 20)
        Me.lblPaisArt.TabIndex = 7
        Me.lblPaisArt.Text = "Pais"
        '
        'lblNombreArt
        '
        Me.lblNombreArt.AutoSize = True
        Me.lblNombreArt.Location = New System.Drawing.Point(250, 69)
        Me.lblNombreArt.Name = "lblNombreArt"
        Me.lblNombreArt.Size = New System.Drawing.Size(65, 20)
        Me.lblNombreArt.TabIndex = 6
        Me.lblNombreArt.Text = "Nombre"
        '
        'lblIdArtista
        '
        Me.lblIdArtista.AutoSize = True
        Me.lblIdArtista.Location = New System.Drawing.Point(250, 25)
        Me.lblIdArtista.Name = "lblIdArtista"
        Me.lblIdArtista.Size = New System.Drawing.Size(76, 20)
        Me.lblIdArtista.TabIndex = 5
        Me.lblIdArtista.Text = "ID Artista"
        '
        'txtNombreArt
        '
        Me.txtNombreArt.Location = New System.Drawing.Point(368, 66)
        Me.txtNombreArt.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtNombreArt.Name = "txtNombreArt"
        Me.txtNombreArt.Size = New System.Drawing.Size(100, 26)
        Me.txtNombreArt.TabIndex = 4
        '
        'txtPaisArt
        '
        Me.txtPaisArt.Location = New System.Drawing.Point(368, 109)
        Me.txtPaisArt.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtPaisArt.Name = "txtPaisArt"
        Me.txtPaisArt.Size = New System.Drawing.Size(100, 26)
        Me.txtPaisArt.TabIndex = 3
        '
        'txtImagenArt
        '
        Me.txtImagenArt.Location = New System.Drawing.Point(368, 154)
        Me.txtImagenArt.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtImagenArt.Name = "txtImagenArt"
        Me.txtImagenArt.Size = New System.Drawing.Size(100, 26)
        Me.txtImagenArt.TabIndex = 2
        '
        'txtIdArt
        '
        Me.txtIdArt.Location = New System.Drawing.Point(368, 22)
        Me.txtIdArt.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtIdArt.Name = "txtIdArt"
        Me.txtIdArt.Size = New System.Drawing.Size(100, 26)
        Me.txtIdArt.TabIndex = 1
        '
        'lstArtistas
        '
        Me.lstArtistas.FormattingEnabled = True
        Me.lstArtistas.ItemHeight = 20
        Me.lstArtistas.Location = New System.Drawing.Point(6, 6)
        Me.lstArtistas.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.lstArtistas.Name = "lstArtistas"
        Me.lstArtistas.Size = New System.Drawing.Size(206, 284)
        Me.lstArtistas.TabIndex = 0
        '
        'tabPCanciones
        '
        Me.tabPCanciones.Controls.Add(Me.lblReproduciendo)
        Me.tabPCanciones.Controls.Add(Me.lblReprosUS)
        Me.tabPCanciones.Controls.Add(Me.lstReproducciones)
        Me.tabPCanciones.Controls.Add(Me.txtUsuarioReproduccion)
        Me.tabPCanciones.Controls.Add(Me.txtFechaReproduccion)
        Me.tabPCanciones.Controls.Add(Me.txtCancionReproduccion)
        Me.tabPCanciones.Controls.Add(Me.txtIdReproduccion)
        Me.tabPCanciones.Controls.Add(Me.txtDuracionCanc)
        Me.tabPCanciones.Controls.Add(Me.txtAlbumCanc)
        Me.tabPCanciones.Controls.Add(Me.txtNombreCanc)
        Me.tabPCanciones.Controls.Add(Me.txtIdCanc)
        Me.tabPCanciones.Controls.Add(Me.lblUsuarioReproduccion)
        Me.tabPCanciones.Controls.Add(Me.btnLimpiarR)
        Me.tabPCanciones.Controls.Add(Me.btnAnadirR)
        Me.tabPCanciones.Controls.Add(Me.lblFechaReproduccion)
        Me.tabPCanciones.Controls.Add(Me.lblCancionReproduccion)
        Me.tabPCanciones.Controls.Add(Me.lblIdReproduccion)
        Me.tabPCanciones.Controls.Add(Me.lstCanOrd)
        Me.tabPCanciones.Controls.Add(Me.lblOrdenarCAN)
        Me.tabPCanciones.Controls.Add(Me.btnOrdenarCAN)
        Me.tabPCanciones.Controls.Add(Me.lblDuracionCanc)
        Me.tabPCanciones.Controls.Add(Me.lblAlbumCanc)
        Me.tabPCanciones.Controls.Add(Me.lblNombreCanc)
        Me.tabPCanciones.Controls.Add(Me.lblIdCanc)
        Me.tabPCanciones.Controls.Add(Me.btnEliminarCancion)
        Me.tabPCanciones.Controls.Add(Me.btnLimpiarCancion)
        Me.tabPCanciones.Controls.Add(Me.btnModificarCancion)
        Me.tabPCanciones.Controls.Add(Me.btnAnadirCancion)
        Me.tabPCanciones.Controls.Add(Me.lstCanciones)
        Me.tabPCanciones.Location = New System.Drawing.Point(4, 29)
        Me.tabPCanciones.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tabPCanciones.Name = "tabPCanciones"
        Me.tabPCanciones.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tabPCanciones.Size = New System.Drawing.Size(1560, 315)
        Me.tabPCanciones.TabIndex = 2
        Me.tabPCanciones.Text = "Canciones"
        Me.tabPCanciones.UseVisualStyleBackColor = True
        '
        'lblReprosUS
        '
        Me.lblReprosUS.AutoSize = True
        Me.lblReprosUS.Location = New System.Drawing.Point(892, 21)
        Me.lblReprosUS.Name = "lblReprosUS"
        Me.lblReprosUS.Size = New System.Drawing.Size(210, 20)
        Me.lblReprosUS.TabIndex = 84
        Me.lblReprosUS.Text = "Reproducciones del usuario:"
        '
        'lstReproducciones
        '
        Me.lstReproducciones.FormattingEnabled = True
        Me.lstReproducciones.ItemHeight = 20
        Me.lstReproducciones.Location = New System.Drawing.Point(896, 55)
        Me.lstReproducciones.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.lstReproducciones.Name = "lstReproducciones"
        Me.lstReproducciones.Size = New System.Drawing.Size(180, 244)
        Me.lstReproducciones.TabIndex = 82
        Me.lstReproducciones.Visible = False
        '
        'txtUsuarioReproduccion
        '
        Me.txtUsuarioReproduccion.Enabled = False
        Me.txtUsuarioReproduccion.Location = New System.Drawing.Point(1217, 63)
        Me.txtUsuarioReproduccion.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtUsuarioReproduccion.Name = "txtUsuarioReproduccion"
        Me.txtUsuarioReproduccion.ReadOnly = True
        Me.txtUsuarioReproduccion.Size = New System.Drawing.Size(142, 26)
        Me.txtUsuarioReproduccion.TabIndex = 81
        Me.txtUsuarioReproduccion.Visible = False
        '
        'txtFechaReproduccion
        '
        Me.txtFechaReproduccion.Location = New System.Drawing.Point(1217, 167)
        Me.txtFechaReproduccion.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtFechaReproduccion.Name = "txtFechaReproduccion"
        Me.txtFechaReproduccion.Size = New System.Drawing.Size(142, 26)
        Me.txtFechaReproduccion.TabIndex = 75
        Me.txtFechaReproduccion.Visible = False
        '
        'txtCancionReproduccion
        '
        Me.txtCancionReproduccion.Location = New System.Drawing.Point(1217, 133)
        Me.txtCancionReproduccion.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtCancionReproduccion.Name = "txtCancionReproduccion"
        Me.txtCancionReproduccion.Size = New System.Drawing.Size(142, 26)
        Me.txtCancionReproduccion.TabIndex = 74
        Me.txtCancionReproduccion.Visible = False
        '
        'txtIdReproduccion
        '
        Me.txtIdReproduccion.Location = New System.Drawing.Point(1217, 99)
        Me.txtIdReproduccion.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtIdReproduccion.Name = "txtIdReproduccion"
        Me.txtIdReproduccion.Size = New System.Drawing.Size(142, 26)
        Me.txtIdReproduccion.TabIndex = 73
        Me.txtIdReproduccion.Visible = False
        '
        'txtDuracionCanc
        '
        Me.txtDuracionCanc.Location = New System.Drawing.Point(416, 171)
        Me.txtDuracionCanc.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtDuracionCanc.Name = "txtDuracionCanc"
        Me.txtDuracionCanc.Size = New System.Drawing.Size(100, 26)
        Me.txtDuracionCanc.TabIndex = 24
        '
        'txtAlbumCanc
        '
        Me.txtAlbumCanc.Location = New System.Drawing.Point(416, 129)
        Me.txtAlbumCanc.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtAlbumCanc.Name = "txtAlbumCanc"
        Me.txtAlbumCanc.Size = New System.Drawing.Size(100, 26)
        Me.txtAlbumCanc.TabIndex = 23
        '
        'txtNombreCanc
        '
        Me.txtNombreCanc.Location = New System.Drawing.Point(416, 91)
        Me.txtNombreCanc.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtNombreCanc.Name = "txtNombreCanc"
        Me.txtNombreCanc.Size = New System.Drawing.Size(100, 26)
        Me.txtNombreCanc.TabIndex = 22
        '
        'txtIdCanc
        '
        Me.txtIdCanc.Location = New System.Drawing.Point(416, 52)
        Me.txtIdCanc.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtIdCanc.Name = "txtIdCanc"
        Me.txtIdCanc.Size = New System.Drawing.Size(100, 26)
        Me.txtIdCanc.TabIndex = 21
        '
        'lblUsuarioReproduccion
        '
        Me.lblUsuarioReproduccion.AutoSize = True
        Me.lblUsuarioReproduccion.Location = New System.Drawing.Point(1083, 69)
        Me.lblUsuarioReproduccion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblUsuarioReproduccion.Name = "lblUsuarioReproduccion"
        Me.lblUsuarioReproduccion.Size = New System.Drawing.Size(64, 20)
        Me.lblUsuarioReproduccion.TabIndex = 80
        Me.lblUsuarioReproduccion.Text = "Usuario"
        Me.lblUsuarioReproduccion.Visible = False
        '
        'btnLimpiarR
        '
        Me.btnLimpiarR.Enabled = False
        Me.btnLimpiarR.Location = New System.Drawing.Point(1251, 207)
        Me.btnLimpiarR.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnLimpiarR.Name = "btnLimpiarR"
        Me.btnLimpiarR.Size = New System.Drawing.Size(95, 34)
        Me.btnLimpiarR.TabIndex = 79
        Me.btnLimpiarR.Text = "Limpiar"
        Me.btnLimpiarR.UseVisualStyleBackColor = True
        Me.btnLimpiarR.Visible = False
        '
        'btnAnadirR
        '
        Me.btnAnadirR.Enabled = False
        Me.btnAnadirR.Location = New System.Drawing.Point(1114, 207)
        Me.btnAnadirR.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnAnadirR.Name = "btnAnadirR"
        Me.btnAnadirR.Size = New System.Drawing.Size(95, 34)
        Me.btnAnadirR.TabIndex = 76
        Me.btnAnadirR.Text = "Añadir"
        Me.btnAnadirR.UseVisualStyleBackColor = True
        Me.btnAnadirR.Visible = False
        '
        'lblFechaReproduccion
        '
        Me.lblFechaReproduccion.AutoSize = True
        Me.lblFechaReproduccion.Location = New System.Drawing.Point(1083, 170)
        Me.lblFechaReproduccion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblFechaReproduccion.Name = "lblFechaReproduccion"
        Me.lblFechaReproduccion.Size = New System.Drawing.Size(54, 20)
        Me.lblFechaReproduccion.TabIndex = 72
        Me.lblFechaReproduccion.Text = "Fecha"
        Me.lblFechaReproduccion.Visible = False
        '
        'lblCancionReproduccion
        '
        Me.lblCancionReproduccion.AutoSize = True
        Me.lblCancionReproduccion.Location = New System.Drawing.Point(1083, 136)
        Me.lblCancionReproduccion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCancionReproduccion.Name = "lblCancionReproduccion"
        Me.lblCancionReproduccion.Size = New System.Drawing.Size(67, 20)
        Me.lblCancionReproduccion.TabIndex = 71
        Me.lblCancionReproduccion.Text = "Cancion"
        Me.lblCancionReproduccion.Visible = False
        '
        'lblIdReproduccion
        '
        Me.lblIdReproduccion.AutoSize = True
        Me.lblIdReproduccion.Location = New System.Drawing.Point(1083, 102)
        Me.lblIdReproduccion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblIdReproduccion.Name = "lblIdReproduccion"
        Me.lblIdReproduccion.Size = New System.Drawing.Size(126, 20)
        Me.lblIdReproduccion.TabIndex = 70
        Me.lblIdReproduccion.Text = "Id Reproduccion"
        Me.lblIdReproduccion.Visible = False
        '
        'lstCanOrd
        '
        Me.lstCanOrd.FormattingEnabled = True
        Me.lstCanOrd.ItemHeight = 20
        Me.lstCanOrd.Location = New System.Drawing.Point(559, 115)
        Me.lstCanOrd.Name = "lstCanOrd"
        Me.lstCanOrd.Size = New System.Drawing.Size(279, 184)
        Me.lstCanOrd.TabIndex = 28
        '
        'lblOrdenarCAN
        '
        Me.lblOrdenarCAN.AutoSize = True
        Me.lblOrdenarCAN.Location = New System.Drawing.Point(602, 12)
        Me.lblOrdenarCAN.Name = "lblOrdenarCAN"
        Me.lblOrdenarCAN.Size = New System.Drawing.Size(183, 40)
        Me.lblOrdenarCAN.TabIndex = 27
        Me.lblOrdenarCAN.Text = "¿Ordenar canciones por " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "todas reproducciones?"
        '
        'btnOrdenarCAN
        '
        Me.btnOrdenarCAN.Location = New System.Drawing.Point(641, 71)
        Me.btnOrdenarCAN.Name = "btnOrdenarCAN"
        Me.btnOrdenarCAN.Size = New System.Drawing.Size(112, 30)
        Me.btnOrdenarCAN.TabIndex = 26
        Me.btnOrdenarCAN.Text = "Ordenar"
        Me.btnOrdenarCAN.UseVisualStyleBackColor = True
        '
        'lblDuracionCanc
        '
        Me.lblDuracionCanc.AutoSize = True
        Me.lblDuracionCanc.Location = New System.Drawing.Point(286, 174)
        Me.lblDuracionCanc.Name = "lblDuracionCanc"
        Me.lblDuracionCanc.Size = New System.Drawing.Size(73, 20)
        Me.lblDuracionCanc.TabIndex = 20
        Me.lblDuracionCanc.Text = "Duracion"
        '
        'lblAlbumCanc
        '
        Me.lblAlbumCanc.AutoSize = True
        Me.lblAlbumCanc.Location = New System.Drawing.Point(286, 132)
        Me.lblAlbumCanc.Name = "lblAlbumCanc"
        Me.lblAlbumCanc.Size = New System.Drawing.Size(75, 20)
        Me.lblAlbumCanc.TabIndex = 19
        Me.lblAlbumCanc.Text = "ID Album"
        '
        'lblNombreCanc
        '
        Me.lblNombreCanc.AutoSize = True
        Me.lblNombreCanc.Location = New System.Drawing.Point(286, 94)
        Me.lblNombreCanc.Name = "lblNombreCanc"
        Me.lblNombreCanc.Size = New System.Drawing.Size(65, 20)
        Me.lblNombreCanc.TabIndex = 18
        Me.lblNombreCanc.Text = "Nombre"
        '
        'lblIdCanc
        '
        Me.lblIdCanc.AutoSize = True
        Me.lblIdCanc.Location = New System.Drawing.Point(286, 55)
        Me.lblIdCanc.Name = "lblIdCanc"
        Me.lblIdCanc.Size = New System.Drawing.Size(88, 20)
        Me.lblIdCanc.TabIndex = 17
        Me.lblIdCanc.Text = "ID Cancion"
        '
        'btnEliminarCancion
        '
        Me.btnEliminarCancion.Enabled = False
        Me.btnEliminarCancion.Location = New System.Drawing.Point(290, 260)
        Me.btnEliminarCancion.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnEliminarCancion.Name = "btnEliminarCancion"
        Me.btnEliminarCancion.Size = New System.Drawing.Size(95, 34)
        Me.btnEliminarCancion.TabIndex = 14
        Me.btnEliminarCancion.Text = "Eliminar"
        Me.btnEliminarCancion.UseVisualStyleBackColor = True
        '
        'btnLimpiarCancion
        '
        Me.btnLimpiarCancion.Enabled = False
        Me.btnLimpiarCancion.Location = New System.Drawing.Point(416, 217)
        Me.btnLimpiarCancion.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnLimpiarCancion.Name = "btnLimpiarCancion"
        Me.btnLimpiarCancion.Size = New System.Drawing.Size(95, 34)
        Me.btnLimpiarCancion.TabIndex = 16
        Me.btnLimpiarCancion.Text = "Limpiar"
        Me.btnLimpiarCancion.UseVisualStyleBackColor = True
        '
        'btnModificarCancion
        '
        Me.btnModificarCancion.Enabled = False
        Me.btnModificarCancion.Location = New System.Drawing.Point(416, 260)
        Me.btnModificarCancion.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnModificarCancion.Name = "btnModificarCancion"
        Me.btnModificarCancion.Size = New System.Drawing.Size(95, 36)
        Me.btnModificarCancion.TabIndex = 15
        Me.btnModificarCancion.Text = "Modificar"
        Me.btnModificarCancion.UseVisualStyleBackColor = True
        '
        'btnAnadirCancion
        '
        Me.btnAnadirCancion.Enabled = False
        Me.btnAnadirCancion.Location = New System.Drawing.Point(290, 217)
        Me.btnAnadirCancion.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnAnadirCancion.Name = "btnAnadirCancion"
        Me.btnAnadirCancion.Size = New System.Drawing.Size(95, 34)
        Me.btnAnadirCancion.TabIndex = 13
        Me.btnAnadirCancion.Text = "Añadir"
        Me.btnAnadirCancion.UseVisualStyleBackColor = True
        '
        'lstCanciones
        '
        Me.lstCanciones.FormattingEnabled = True
        Me.lstCanciones.ItemHeight = 20
        Me.lstCanciones.Location = New System.Drawing.Point(10, 12)
        Me.lstCanciones.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.lstCanciones.Name = "lstCanciones"
        Me.lstCanciones.Size = New System.Drawing.Size(250, 284)
        Me.lstCanciones.TabIndex = 0
        '
        'tabPAlbumes
        '
        Me.tabPAlbumes.Controls.Add(Me.lstCancionesAlbum)
        Me.tabPAlbumes.Controls.Add(Me.lblCancionesAlbum)
        Me.tabPAlbumes.Controls.Add(Me.pbAlbumes)
        Me.tabPAlbumes.Controls.Add(Me.lblDuracionAlbum)
        Me.tabPAlbumes.Controls.Add(Me.txtDuracionAlbum)
        Me.tabPAlbumes.Controls.Add(Me.txtPortadaAlbum)
        Me.tabPAlbumes.Controls.Add(Me.txtIdAlbum)
        Me.tabPAlbumes.Controls.Add(Me.txtNombreAlbum)
        Me.tabPAlbumes.Controls.Add(Me.txtFechaAlbum)
        Me.tabPAlbumes.Controls.Add(Me.txtIdArtistaAlb)
        Me.tabPAlbumes.Controls.Add(Me.lblImgAlbum)
        Me.tabPAlbumes.Controls.Add(Me.lstAlbumes)
        Me.tabPAlbumes.Controls.Add(Me.lblIdAlbum)
        Me.tabPAlbumes.Controls.Add(Me.lblNombreAlbum)
        Me.tabPAlbumes.Controls.Add(Me.btnEliminarAlbum)
        Me.tabPAlbumes.Controls.Add(Me.btnLimpiarAlbum)
        Me.tabPAlbumes.Controls.Add(Me.lblIdArtistaAlb)
        Me.tabPAlbumes.Controls.Add(Me.btnModificarAlbum)
        Me.tabPAlbumes.Controls.Add(Me.lblFechaAlbum)
        Me.tabPAlbumes.Controls.Add(Me.btnAnadirAlbum)
        Me.tabPAlbumes.Location = New System.Drawing.Point(4, 29)
        Me.tabPAlbumes.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tabPAlbumes.Name = "tabPAlbumes"
        Me.tabPAlbumes.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.tabPAlbumes.Size = New System.Drawing.Size(1560, 315)
        Me.tabPAlbumes.TabIndex = 3
        Me.tabPAlbumes.Text = "Albumes"
        Me.tabPAlbumes.UseVisualStyleBackColor = True
        '
        'lstCancionesAlbum
        '
        Me.lstCancionesAlbum.FormattingEnabled = True
        Me.lstCancionesAlbum.ItemHeight = 20
        Me.lstCancionesAlbum.Location = New System.Drawing.Point(905, 58)
        Me.lstCancionesAlbum.Name = "lstCancionesAlbum"
        Me.lstCancionesAlbum.Size = New System.Drawing.Size(188, 144)
        Me.lstCancionesAlbum.TabIndex = 38
        '
        'lblCancionesAlbum
        '
        Me.lblCancionesAlbum.AutoSize = True
        Me.lblCancionesAlbum.Location = New System.Drawing.Point(901, 22)
        Me.lblCancionesAlbum.Name = "lblCancionesAlbum"
        Me.lblCancionesAlbum.Size = New System.Drawing.Size(192, 20)
        Me.lblCancionesAlbum.TabIndex = 37
        Me.lblCancionesAlbum.Text = "Canciones de este album:"
        '
        'pbAlbumes
        '
        Me.pbAlbumes.Location = New System.Drawing.Point(666, 22)
        Me.pbAlbumes.Name = "pbAlbumes"
        Me.pbAlbumes.Size = New System.Drawing.Size(184, 191)
        Me.pbAlbumes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbAlbumes.TabIndex = 36
        Me.pbAlbumes.TabStop = False
        Me.pbAlbumes.WaitOnLoad = True
        '
        'lblDuracionAlbum
        '
        Me.lblDuracionAlbum.AutoSize = True
        Me.lblDuracionAlbum.Location = New System.Drawing.Point(291, 212)
        Me.lblDuracionAlbum.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDuracionAlbum.Name = "lblDuracionAlbum"
        Me.lblDuracionAlbum.Size = New System.Drawing.Size(73, 20)
        Me.lblDuracionAlbum.TabIndex = 35
        Me.lblDuracionAlbum.Text = "Duracion"
        '
        'txtDuracionAlbum
        '
        Me.txtDuracionAlbum.Enabled = False
        Me.txtDuracionAlbum.Location = New System.Drawing.Point(484, 209)
        Me.txtDuracionAlbum.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtDuracionAlbum.Name = "txtDuracionAlbum"
        Me.txtDuracionAlbum.Size = New System.Drawing.Size(122, 26)
        Me.txtDuracionAlbum.TabIndex = 31
        '
        'txtPortadaAlbum
        '
        Me.txtPortadaAlbum.Location = New System.Drawing.Point(484, 169)
        Me.txtPortadaAlbum.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtPortadaAlbum.Name = "txtPortadaAlbum"
        Me.txtPortadaAlbum.Size = New System.Drawing.Size(122, 26)
        Me.txtPortadaAlbum.TabIndex = 30
        '
        'txtIdAlbum
        '
        Me.txtIdAlbum.Location = New System.Drawing.Point(484, 22)
        Me.txtIdAlbum.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtIdAlbum.Name = "txtIdAlbum"
        Me.txtIdAlbum.Size = New System.Drawing.Size(122, 26)
        Me.txtIdAlbum.TabIndex = 16
        '
        'txtNombreAlbum
        '
        Me.txtNombreAlbum.Location = New System.Drawing.Point(484, 58)
        Me.txtNombreAlbum.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtNombreAlbum.Name = "txtNombreAlbum"
        Me.txtNombreAlbum.Size = New System.Drawing.Size(122, 26)
        Me.txtNombreAlbum.TabIndex = 17
        '
        'txtFechaAlbum
        '
        Me.txtFechaAlbum.Location = New System.Drawing.Point(484, 95)
        Me.txtFechaAlbum.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtFechaAlbum.Name = "txtFechaAlbum"
        Me.txtFechaAlbum.Size = New System.Drawing.Size(122, 26)
        Me.txtFechaAlbum.TabIndex = 26
        '
        'txtIdArtistaAlb
        '
        Me.txtIdArtistaAlb.Location = New System.Drawing.Point(484, 131)
        Me.txtIdArtistaAlb.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtIdArtistaAlb.Name = "txtIdArtistaAlb"
        Me.txtIdArtistaAlb.Size = New System.Drawing.Size(122, 26)
        Me.txtIdArtistaAlb.TabIndex = 27
        '
        'lblImgAlbum
        '
        Me.lblImgAlbum.AutoSize = True
        Me.lblImgAlbum.Location = New System.Drawing.Point(291, 174)
        Me.lblImgAlbum.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblImgAlbum.Name = "lblImgAlbum"
        Me.lblImgAlbum.Size = New System.Drawing.Size(65, 20)
        Me.lblImgAlbum.TabIndex = 29
        Me.lblImgAlbum.Text = "Portada"
        '
        'lstAlbumes
        '
        Me.lstAlbumes.FormattingEnabled = True
        Me.lstAlbumes.ItemHeight = 20
        Me.lstAlbumes.Location = New System.Drawing.Point(6, 6)
        Me.lstAlbumes.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.lstAlbumes.Name = "lstAlbumes"
        Me.lstAlbumes.Size = New System.Drawing.Size(252, 284)
        Me.lstAlbumes.TabIndex = 28
        '
        'lblIdAlbum
        '
        Me.lblIdAlbum.AutoSize = True
        Me.lblIdAlbum.Location = New System.Drawing.Point(291, 25)
        Me.lblIdAlbum.Name = "lblIdAlbum"
        Me.lblIdAlbum.Size = New System.Drawing.Size(75, 20)
        Me.lblIdAlbum.TabIndex = 22
        Me.lblIdAlbum.Text = "ID Album"
        '
        'lblNombreAlbum
        '
        Me.lblNombreAlbum.AutoSize = True
        Me.lblNombreAlbum.Location = New System.Drawing.Point(291, 62)
        Me.lblNombreAlbum.Name = "lblNombreAlbum"
        Me.lblNombreAlbum.Size = New System.Drawing.Size(112, 20)
        Me.lblNombreAlbum.TabIndex = 23
        Me.lblNombreAlbum.Text = "Nombre album"
        '
        'btnEliminarAlbum
        '
        Me.btnEliminarAlbum.Enabled = False
        Me.btnEliminarAlbum.Location = New System.Drawing.Point(446, 248)
        Me.btnEliminarAlbum.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnEliminarAlbum.Name = "btnEliminarAlbum"
        Me.btnEliminarAlbum.Size = New System.Drawing.Size(118, 42)
        Me.btnEliminarAlbum.TabIndex = 19
        Me.btnEliminarAlbum.Text = "Eliminar"
        Me.btnEliminarAlbum.UseVisualStyleBackColor = True
        '
        'btnLimpiarAlbum
        '
        Me.btnLimpiarAlbum.Enabled = False
        Me.btnLimpiarAlbum.Location = New System.Drawing.Point(590, 248)
        Me.btnLimpiarAlbum.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnLimpiarAlbum.Name = "btnLimpiarAlbum"
        Me.btnLimpiarAlbum.Size = New System.Drawing.Size(118, 42)
        Me.btnLimpiarAlbum.TabIndex = 21
        Me.btnLimpiarAlbum.Text = "Limpiar"
        Me.btnLimpiarAlbum.UseVisualStyleBackColor = True
        '
        'lblIdArtistaAlb
        '
        Me.lblIdArtistaAlb.AutoSize = True
        Me.lblIdArtistaAlb.Location = New System.Drawing.Point(291, 135)
        Me.lblIdArtistaAlb.Name = "lblIdArtistaAlb"
        Me.lblIdArtistaAlb.Size = New System.Drawing.Size(76, 20)
        Me.lblIdArtistaAlb.TabIndex = 25
        Me.lblIdArtistaAlb.Text = "ID Artista"
        '
        'btnModificarAlbum
        '
        Me.btnModificarAlbum.Enabled = False
        Me.btnModificarAlbum.Location = New System.Drawing.Point(738, 248)
        Me.btnModificarAlbum.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnModificarAlbum.Name = "btnModificarAlbum"
        Me.btnModificarAlbum.Size = New System.Drawing.Size(118, 42)
        Me.btnModificarAlbum.TabIndex = 20
        Me.btnModificarAlbum.Text = "Modificar"
        Me.btnModificarAlbum.UseVisualStyleBackColor = True
        '
        'lblFechaAlbum
        '
        Me.lblFechaAlbum.AutoSize = True
        Me.lblFechaAlbum.Location = New System.Drawing.Point(291, 100)
        Me.lblFechaAlbum.Name = "lblFechaAlbum"
        Me.lblFechaAlbum.Size = New System.Drawing.Size(54, 20)
        Me.lblFechaAlbum.TabIndex = 24
        Me.lblFechaAlbum.Text = "Fecha"
        '
        'btnAnadirAlbum
        '
        Me.btnAnadirAlbum.Enabled = False
        Me.btnAnadirAlbum.Location = New System.Drawing.Point(296, 248)
        Me.btnAnadirAlbum.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnAnadirAlbum.Name = "btnAnadirAlbum"
        Me.btnAnadirAlbum.Size = New System.Drawing.Size(118, 45)
        Me.btnAnadirAlbum.TabIndex = 18
        Me.btnAnadirAlbum.Text = "Añadir"
        Me.btnAnadirAlbum.UseVisualStyleBackColor = True
        '
        'btnRutaBD
        '
        Me.btnRutaBD.Location = New System.Drawing.Point(28, 425)
        Me.btnRutaBD.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnRutaBD.Name = "btnRutaBD"
        Me.btnRutaBD.Size = New System.Drawing.Size(94, 48)
        Me.btnRutaBD.TabIndex = 1
        Me.btnRutaBD.Text = "RutaBD"
        Me.btnRutaBD.UseVisualStyleBackColor = True
        '
        'btnConectar
        '
        Me.btnConectar.Enabled = False
        Me.btnConectar.Location = New System.Drawing.Point(28, 505)
        Me.btnConectar.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnConectar.Name = "btnConectar"
        Me.btnConectar.Size = New System.Drawing.Size(94, 46)
        Me.btnConectar.TabIndex = 2
        Me.btnConectar.Text = "Conectar"
        Me.btnConectar.UseVisualStyleBackColor = True
        '
        'txtRuta
        '
        Me.txtRuta.Location = New System.Drawing.Point(160, 425)
        Me.txtRuta.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtRuta.Multiline = True
        Me.txtRuta.Name = "txtRuta"
        Me.txtRuta.ReadOnly = True
        Me.txtRuta.Size = New System.Drawing.Size(570, 73)
        Me.txtRuta.TabIndex = 13
        '
        'lblConectar
        '
        Me.lblConectar.AutoSize = True
        Me.lblConectar.Location = New System.Drawing.Point(158, 518)
        Me.lblConectar.Name = "lblConectar"
        Me.lblConectar.Size = New System.Drawing.Size(29, 20)
        Me.lblConectar.TabIndex = 14
        Me.lblConectar.Text = ". . ."
        '
        'ofdRuta
        '
        Me.ofdRuta.FileName = "OpenFileDialog1"
        '
        'lblReproduciendo
        '
        Me.lblReproduciendo.AutoSize = True
        Me.lblReproduciendo.Location = New System.Drawing.Point(1110, 267)
        Me.lblReproduciendo.Name = "lblReproduciendo"
        Me.lblReproduciendo.Size = New System.Drawing.Size(29, 20)
        Me.lblReproduciendo.TabIndex = 85
        Me.lblReproduciendo.Text = ". . ."
        '
        'CANCIONIFY
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(1478, 571)
        Me.Controls.Add(Me.lblConectar)
        Me.Controls.Add(Me.txtRuta)
        Me.Controls.Add(Me.btnConectar)
        Me.Controls.Add(Me.btnRutaBD)
        Me.Controls.Add(Me.gbCancionfy)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "CANCIONIFY"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "CANCIONIFY"
        Me.gbCancionfy.ResumeLayout(False)
        Me.gbCancionfy.PerformLayout()
        Me.tabPCancionify.ResumeLayout(False)
        Me.tabPUsuarios.ResumeLayout(False)
        Me.tabPUsuarios.PerformLayout()
        Me.tabPArtistas.ResumeLayout(False)
        Me.tabPArtistas.PerformLayout()
        CType(Me.pbArtista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabPCanciones.ResumeLayout(False)
        Me.tabPCanciones.PerformLayout()
        Me.tabPAlbumes.ResumeLayout(False)
        Me.tabPAlbumes.PerformLayout()
        CType(Me.pbAlbumes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents gbCancionfy As GroupBox
    Friend WithEvents btnRutaBD As Button
    Friend WithEvents btnConectar As Button
    Friend WithEvents txtRuta As TextBox
    Friend WithEvents lblConectar As Label
    Friend WithEvents ofdRuta As OpenFileDialog
    Friend WithEvents lblUsuarioSeleccionadoInfo As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents tabPCancionify As TabControl
    Friend WithEvents tabPUsuarios As TabPage
    Friend WithEvents btnMostrarUsMasT As Button
    Friend WithEvents lblUsMasT As Label
    Friend WithEvents lstUsMasT As ListBox
    Friend WithEvents lblHasta As Label
    Friend WithEvents lblDesde As Label
    Friend WithEvents dtpHasta As DateTimePicker
    Friend WithEvents dtpDesde As DateTimePicker
    Friend WithEvents btnARTMasEscuchadoUS As Button
    Friend WithEvents lblArtMEscuchadosUS As Label
    Friend WithEvents lstArtMEscuchadosUS As ListBox
    Friend WithEvents lstUsuarios As ListBox
    Friend WithEvents txtEmail As TextBox
    Friend WithEvents txtNombre As TextBox
    Friend WithEvents txtApellidos As TextBox
    Friend WithEvents txtFechaNac As TextBox
    Friend WithEvents lblEmail As Label
    Friend WithEvents lblNombre As Label
    Friend WithEvents btnEliminarUsuario As Button
    Friend WithEvents btnLimpiarUsuario As Button
    Friend WithEvents lblFechaNac As Label
    Friend WithEvents btnModificarUsuario As Button
    Friend WithEvents lblApellidos As Label
    Friend WithEvents btnAnadirUsuario As Button
    Friend WithEvents tabPArtistas As TabPage
    Friend WithEvents Label5 As Label
    Friend WithEvents btnTReproArtFav As Button
    Friend WithEvents lstTReproArtFav As ListBox
    Friend WithEvents lblEsFavorito As Label
    Friend WithEvents btnQuitarFav As Button
    Friend WithEvents btnFavorito As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents lstAlbumesART As ListBox
    Friend WithEvents pbArtista As PictureBox
    Friend WithEvents lstArtistasFav As ListBox
    Friend WithEvents btnEliminarArtista As Button
    Friend WithEvents btnLimpiarArtista As Button
    Friend WithEvents btnModificarArtista As Button
    Friend WithEvents btnAnadirArtista As Button
    Friend WithEvents lblImagenArt As Label
    Friend WithEvents lblPaisArt As Label
    Friend WithEvents lblNombreArt As Label
    Friend WithEvents lblIdArtista As Label
    Friend WithEvents txtNombreArt As TextBox
    Friend WithEvents txtPaisArt As TextBox
    Friend WithEvents txtImagenArt As TextBox
    Friend WithEvents txtIdArt As TextBox
    Friend WithEvents lstArtistas As ListBox
    Friend WithEvents tabPCanciones As TabPage
    Friend WithEvents lblReprosUS As Label
    Friend WithEvents lstReproducciones As ListBox
    Friend WithEvents txtUsuarioReproduccion As TextBox
    Friend WithEvents txtFechaReproduccion As TextBox
    Friend WithEvents txtCancionReproduccion As TextBox
    Friend WithEvents txtIdReproduccion As TextBox
    Friend WithEvents txtDuracionCanc As TextBox
    Friend WithEvents txtAlbumCanc As TextBox
    Friend WithEvents txtNombreCanc As TextBox
    Friend WithEvents txtIdCanc As TextBox
    Friend WithEvents lblUsuarioReproduccion As Label
    Friend WithEvents btnLimpiarR As Button
    Friend WithEvents btnAnadirR As Button
    Friend WithEvents lblFechaReproduccion As Label
    Friend WithEvents lblCancionReproduccion As Label
    Friend WithEvents lblIdReproduccion As Label
    Friend WithEvents lstCanOrd As ListBox
    Friend WithEvents lblOrdenarCAN As Label
    Friend WithEvents btnOrdenarCAN As Button
    Friend WithEvents lblDuracionCanc As Label
    Friend WithEvents lblAlbumCanc As Label
    Friend WithEvents lblNombreCanc As Label
    Friend WithEvents lblIdCanc As Label
    Friend WithEvents btnEliminarCancion As Button
    Friend WithEvents btnLimpiarCancion As Button
    Friend WithEvents btnModificarCancion As Button
    Friend WithEvents btnAnadirCancion As Button
    Friend WithEvents lstCanciones As ListBox
    Friend WithEvents tabPAlbumes As TabPage
    Friend WithEvents lstCancionesAlbum As ListBox
    Friend WithEvents lblCancionesAlbum As Label
    Friend WithEvents pbAlbumes As PictureBox
    Friend WithEvents lblDuracionAlbum As Label
    Friend WithEvents txtDuracionAlbum As TextBox
    Friend WithEvents txtPortadaAlbum As TextBox
    Friend WithEvents txtIdAlbum As TextBox
    Friend WithEvents txtNombreAlbum As TextBox
    Friend WithEvents txtFechaAlbum As TextBox
    Friend WithEvents txtIdArtistaAlb As TextBox
    Friend WithEvents lblImgAlbum As Label
    Friend WithEvents lstAlbumes As ListBox
    Friend WithEvents lblIdAlbum As Label
    Friend WithEvents lblNombreAlbum As Label
    Friend WithEvents btnEliminarAlbum As Button
    Friend WithEvents btnLimpiarAlbum As Button
    Friend WithEvents lblIdArtistaAlb As Label
    Friend WithEvents btnModificarAlbum As Button
    Friend WithEvents lblFechaAlbum As Label
    Friend WithEvents btnAnadirAlbum As Button
    Friend WithEvents lblReproduciendo As Label
End Class
