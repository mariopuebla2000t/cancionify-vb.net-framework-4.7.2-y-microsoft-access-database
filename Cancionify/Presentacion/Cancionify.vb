﻿Public Class CANCIONIFY

    Dim UsuarioS As Usuario
    Dim cancionS As Collection
    Dim reproducciones As Collection

    Private Sub btnRutaBD_Click(sender As Object, e As EventArgs) Handles btnRutaBD.Click
        ofdRuta.InitialDirectory = Application.StartupPath
        If ofdRuta.ShowDialog() = DialogResult.OK Then
            txtRuta.Text = ofdRuta.FileName.ToString
            btnConectar.Enabled = True
        End If

    End Sub

    Private Sub btnConectar_Click(sender As Object, e As EventArgs) Handles btnConectar.Click
        Dim USux As Usuario = New Usuario
        Dim ARux As Artista = New Artista
        Dim CAux As Cancion = New Cancion
        Dim ABux As Album = New Album
        Try
            USux.leerTodoUsuario(txtRuta.Text)
            ARux.leerTodoArtista(txtRuta.Text)
            CAux.leerTodoCancion(txtRuta.Text)
            ABux.leerTodoAlbum(txtRuta.Text)
        Catch ex As Exception
            MessageBox.Show("La bbdd tiene que se cancionify.accdb, cierre la aplicación y vuelva a intentarlo", "Error fatal", MessageBoxButtons.OK, MessageBoxIcon.Error)
            lblConectar.Text = "Error de conexion"
            lblConectar.ForeColor = Color.Red
            btnRutaBD.Enabled = False
            btnConectar.Enabled = False
            Exit Sub
        End Try
        For Each U As Usuario In USux.UsDAO.Usuarios
            lstUsuarios.Items.Add(U.emailUS)
        Next
        For Each A As Artista In ARux.ArDAO.Artistas
            lstArtistas.Items.Add(A.idArtistaART)
        Next
        For Each C As Cancion In CAux.CanDAO.Canciones
            lstCanciones.Items.Add(C.idCANC)
        Next
        For Each Ab As Album In ABux.AlbDAO.Albumes
            lstAlbumes.Items.Add(Ab.idALB)
        Next
        btnRutaBD.Enabled = False
        btnConectar.Enabled = False
        gbCancionfy.Enabled = True

        tabPAlbumes.Enabled = False
        tabPArtistas.Enabled = False
        tabPCanciones.Enabled = False
        tabPUsuarios.Enabled = True

        'Enable inicial de Usuario'
        btnEliminarUsuario.Enabled = True
        btnModificarUsuario.Enabled = True
        btnAnadirUsuario.Enabled = True
        btnLimpiarUsuario.Enabled = True

        'Enable inicial de Artista'
        btnAnadirArtista.Enabled = True
        btnModificarArtista.Enabled = True
        btnEliminarArtista.Enabled = True
        btnLimpiarArtista.Enabled = True

        'Enable inicial de Cancion'
        btnAnadirCancion.Enabled = True
        btnModificarCancion.Enabled = True
        btnEliminarCancion.Enabled = True
        btnLimpiarCancion.Enabled = True

        'Enable inicial de Album'
        btnAnadirAlbum.Enabled = True
        btnModificarAlbum.Enabled = True
        btnEliminarAlbum.Enabled = True
        btnLimpiarAlbum.Enabled = True

        'Datos de Usuario'
        txtEmail.Enabled = True
        txtNombre.Enabled = True
        txtApellidos.Enabled = True
        txtFechaNac.Enabled = True

        'Datos de Artista'
        txtIdArt.Enabled = True
        txtNombreArt.Enabled = True
        txtPaisArt.Enabled = True
        txtImagenArt.Enabled = True

        'Datos de Cancion'
        txtIdCanc.Enabled = True
        txtNombreCanc.Enabled = True
        txtDuracionCanc.Enabled = True
        txtAlbumCanc.Enabled = True

        'Datos de Album'
        txtIdAlbum.Enabled = True
        txtNombreAlbum.Enabled = True
        txtFechaAlbum.Enabled = True
        txtIdArtistaAlb.Enabled = True
        txtPortadaAlbum.Enabled = True

        lblConectar.Text = "Conexión correcta"
        lblConectar.ForeColor = Color.Green
    End Sub

    Private Sub lstUsuarios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstUsuarios.SelectedIndexChanged
        Dim USux As Usuario
        Dim fechaDeHoy As Date
        If lstUsuarios.SelectedItem IsNot Nothing Then
            USux = New Usuario(lstUsuarios.SelectedItem.ToString())
            Try
                USux.leerUsuario()

            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
            UsuarioS = USux
            gbCancionfy.Enabled = True
            txtEmail.Text = USux.emailUS
            lblUsuarioSeleccionadoInfo.Text = USux.emailUS
            txtNombre.Text = USux.nombreUS
            txtApellidos.Text = USux.apellidoUS
            txtFechaNac.Text = USux.fechaNacimientoUS

            'Enable de los botones en Usuario'
            btnAnadirUsuario.Enabled = False
            btnModificarUsuario.Enabled = True
            btnEliminarUsuario.Enabled = True
            btnLimpiarUsuario.Enabled = True

            tabPAlbumes.Enabled = True
            tabPArtistas.Enabled = True
            tabPCanciones.Enabled = True

            'Mostramos las reproducciones del usuario'

            Dim RAux As Reproduccion = New Reproduccion
            Try
                RAux.leerTodoReproduccion(USux)
            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try

            lstReproducciones.Visible = True

            lstReproducciones.Items.Clear()
            txtIdReproduccion.Clear()
            txtCancionReproduccion.Clear()

            fechaDeHoy = Today
            txtFechaReproduccion.Text = fechaDeHoy

            For Each R As Reproduccion In RAux.ReDAO.Reproducciones
                lstReproducciones.Items.Add(R.IdReproduccion)
            Next

            'For Each R In RAux.ReDAO.Reproducciones
            '    txtIdReproduccion.Text = RAux._idReproduccion
            '    txtCancionReproduccion.Text = RAux._cancionReproducion.nombreCANC
            '    txtFechaReproduccion.Text = RAux._fechaReproduccion
            'Next

            lstArtistasFav.Items.Clear()

            Dim ArtAUX As Artista = New Artista
            Try
                ArtAUX.leerTodoArtista(txtRuta.Text)
            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try

            lstArtistasFav.Visible = True

            For Each A As Artista In ArtAUX.ArDAO.Artistas
                If A.favorito(UsuarioS) Then
                    lstArtistasFav.Items.Add(A.idArtistaART)
                End If
            Next

            lblEsFavorito.Text = ""

            lblUsuarioReproduccion.Visible = True
            lblIdReproduccion.Visible = True
            lblCancionReproduccion.Visible = True
            lblFechaReproduccion.Visible = True

            txtUsuarioReproduccion.Visible = True
            txtIdReproduccion.Visible = True
            txtCancionReproduccion.Visible = True
            txtFechaReproduccion.Visible = True

            btnAnadirR.Visible = True
            btnLimpiarR.Visible = True

            btnAnadirR.Enabled = True
            btnLimpiarR.Enabled = True

            txtUsuarioReproduccion.Text = USux.emailUS
        End If
    End Sub

    Private Sub lstReproducciones_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstReproducciones.SelectedIndexChanged
        Dim RAux As Reproduccion
        If lstReproducciones.SelectedItem IsNot Nothing Then
            RAux = New Reproduccion(lstReproducciones.SelectedItem)
            Try
                RAux.leerReproduccion()
            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try

            txtIdReproduccion.Text = CType(RAux.IdReproduccion, String)
            txtCancionReproduccion.Text = RAux.cancionRepro.idCANC
            txtFechaReproduccion.Text = RAux.fechaRepro

        End If
    End Sub

    Private Sub lstArtistas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstArtistas.SelectedIndexChanged
        Dim ARux As Artista
        If lstArtistas.SelectedItem IsNot Nothing Then
            ARux = New Artista(lstArtistas.SelectedItem.ToString())
            Try
                ARux.leerArtista()

            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
            txtIdArt.Text = CType(ARux.idArtistaART, String)
            txtNombreArt.Text = ARux.nombreART
            txtPaisArt.Text = ARux.paisART
            txtImagenArt.Text = ARux.imagenART

            Try
                pbArtista.Load(ARux.imagenART)
            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

            'Enable en los botones de Artista'
            btnAnadirArtista.Enabled = False
            btnModificarArtista.Enabled = True
            btnEliminarArtista.Enabled = True
            btnLimpiarArtista.Enabled = True

            If ARux.favorito(UsuarioS) Then
                lblEsFavorito.Text = "Artista marcado como favorito"
                lblEsFavorito.ForeColor = Color.Green
            Else
                lblEsFavorito.Text = "Artista NO marcado como favorito"
                lblEsFavorito.ForeColor = Color.Red
            End If

            lstAlbumesART.Items.Clear()

            Dim ABux As Album = New Album
            Try
                ABux.leerTodoAlbum(txtRuta.Text)
            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
            For Each Ab As Album In ABux.AlbDAO.Albumes
                If Ab.idArtistaALB = ARux.idArtistaART Then
                    lstAlbumesART.Items.Add(Ab.nombreALB)
                End If
            Next

        End If
    End Sub

    Private Sub lstCanciones_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstCanciones.SelectedIndexChanged
        Dim CAux As Cancion
        If lstCanciones.SelectedItem IsNot Nothing Then
            CAux = New Cancion(lstCanciones.SelectedItem.ToString())
            Try
                CAux.leerCancion()

            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
            txtIdCanc.Text = CType(CAux.idCANC, String)
            txtNombreCanc.Text = CAux.nombreCANC
            txtAlbumCanc.Text = CType(CAux.albumCANC, String)
            txtDuracionCanc.Text = TimeSpan.FromSeconds(CAux.duracionCANC).ToString

            'Enable de los botones en Cancion'
            btnAnadirCancion.Enabled = False
            btnModificarCancion.Enabled = True
            btnEliminarCancion.Enabled = True
            btnLimpiarCancion.Enabled = True
            txtDuracionCanc.ReadOnly = True
        End If
    End Sub

    Private Sub lstAlbumes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstAlbumes.SelectedIndexChanged
        Dim ABux As Album
        If lstAlbumes.SelectedItem IsNot Nothing Then
            ABux = New Album(lstAlbumes.SelectedItem.ToString())
            Try
                ABux.leerAlbum()
            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
            txtIdAlbum.Text = CType(ABux.idALB, String)
            txtNombreAlbum.Text = ABux.nombreALB
            txtFechaAlbum.Text = ABux.fechaALB
            txtIdArtistaAlb.Text = CType(ABux.idArtistaALB, String)
            txtPortadaAlbum.Text = ABux.portadaALB

            calcularDuracionAlbum(ABux)

            Try
                pbAlbumes.Load(ABux.portadaALB)
            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

            'Enable botones Album'
            btnAnadirAlbum.Enabled = False
            btnModificarAlbum.Enabled = True
            btnEliminarAlbum.Enabled = True
            btnLimpiarAlbum.Enabled = True

            lstCancionesAlbum.Items.Clear()

            Dim CAux As Cancion = New Cancion
            Try
                CAux.leerTodoCancion(txtRuta.Text)
            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
            For Each c As Cancion In CAux.CanDAO.Canciones
                If c.albumCANC = ABux.idALB Then
                    lstCancionesAlbum.Items.Add(c.nombreCANC)
                End If
            Next
        End If

    End Sub

    Private Sub calcularDuracionAlbum(ABux As Album)
        Dim CAux As Cancion = New Cancion
        Dim duracion As Integer = 0
        Try
            CAux.leerTodoCancion(txtRuta.Text)
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
            lblConectar.Text = "Error de conexion"
            lblConectar.ForeColor = Color.Red
            Exit Sub
        End Try
        For Each C As Cancion In CAux.CanDAO.Canciones
            If C.albumCANC = ABux.idALB Then
                duracion = duracion + C.duracionCANC
            End If
        Next
        txtDuracionAlbum.Text = TimeSpan.FromSeconds(duracion).ToString

    End Sub

    'METODOS DE LOS BOTONES DE USUARIO'
    Private Sub btnLimpiarUsuario_Click(sender As Object, e As EventArgs) Handles btnLimpiarUsuario.Click
        txtEmail.Text = String.Empty
        txtNombre.Text = String.Empty
        txtApellidos.Text = String.Empty
        txtFechaNac.Text = String.Empty
        btnAnadirUsuario.Enabled = True
        btnModificarUsuario.Enabled = False
        btnEliminarUsuario.Enabled = False
    End Sub

    Private Sub btnAnadirUsuario_Click(sender As Object, e As EventArgs) Handles btnAnadirUsuario.Click
        Dim USux As Usuario
        If txtEmail.Text IsNot String.Empty And txtNombre.Text IsNot String.Empty And txtApellidos.Text IsNot String.Empty And txtFechaNac.Text IsNot String.Empty Then
            USux = New Usuario(txtEmail.Text, txtNombre.Text, txtApellidos.Text, txtFechaNac.Text)
            Try
                If USux.insertarUsuario() <> 1 Then
                    MessageBox.Show("Insert return distinto de 1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
            lstUsuarios.Items.Add(USux.emailUS)
            MessageBox.Show("Usuario con el email: " & USux.emailUS & " ha sido añadido correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub btnModificarUsuario_Click(sender As Object, e As EventArgs) Handles btnModificarUsuario.Click
        Dim USux As Usuario
        If txtEmail.Text IsNot String.Empty And txtNombre.Text IsNot String.Empty And txtApellidos.Text IsNot String.Empty And txtFechaNac.Text IsNot String.Empty Then
            USux = New Usuario(txtEmail.Text, txtNombre.Text, txtApellidos.Text, txtFechaNac.Text)
            Try
                If USux.modificarUsuario() <> 1 Then
                    MessageBox.Show("Update return distinto de 1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
            MessageBox.Show("Usuario con el email: " & USux.emailUS & " ha sido actualizado correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub btnEliminarUsuario_Click(sender As Object, e As EventArgs) Handles btnEliminarUsuario.Click
        Dim USux As Usuario
        If txtEmail.Text IsNot String.Empty And txtNombre.Text IsNot String.Empty And txtApellidos.Text IsNot String.Empty And txtFechaNac.Text IsNot String.Empty Then
            USux = New Usuario(txtEmail.Text, txtNombre.Text, txtApellidos.Text, txtFechaNac.Text)
            If MessageBox.Show("¿Esta seguro de eliminar el Usuario con email: " & USux.emailUS & " ?", "Correcto", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then

                USux = New Usuario(txtEmail.Text, txtNombre.Text, txtApellidos.Text, txtFechaNac.Text)
                Try
                    If USux.eliminarUsuario() <> 1 Then
                        MessageBox.Show("Delete return distinto de 1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End If
                Catch ex As Exception
                    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End Try
                lstUsuarios.Items.Remove(USux.emailUS)
                btnLimpiarUsuario.PerformClick()
                MessageBox.Show("Usuario eliminado correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    'METODOS DE LOS BOTONES DE ARTISTA'
    Private Sub btnAnadirArtista_Click(sender As Object, e As EventArgs) Handles btnAnadirArtista.Click
        Dim ARux As Artista
        If txtIdArt.Text IsNot String.Empty And txtNombreArt IsNot String.Empty And txtPaisArt.Text IsNot String.Empty And txtImagenArt.Text IsNot String.Empty Then
            ARux = New Artista(txtIdArt.Text, txtNombreArt.Text, txtPaisArt.Text, txtImagenArt.Text)

            Try
                If ARux.insertarArtista() <> 1 Then
                    MessageBox.Show("Insert return distinto de 1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
            lstArtistas.Items.Add(ARux.idArtistaART)
            MessageBox.Show("Artista con el id: " & ARux.idArtistaART & " ha sido añadido correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub btnLimpiarArtista_Click(sender As Object, e As EventArgs) Handles btnLimpiarArtista.Click
        txtIdArt.Text = String.Empty
        txtNombreArt.Text = String.Empty
        txtPaisArt.Text = String.Empty
        txtImagenArt.Text = String.Empty
        pbArtista.Image.Dispose()
        pbArtista.Image = Nothing
        btnAnadirArtista.Enabled = True
        btnModificarArtista.Enabled = False
        btnEliminarArtista.Enabled = False
        lstAlbumesART.Items.Clear()
        lblEsFavorito.Text = " "
    End Sub

    Private Sub btnEliminarArtista_Click(sender As Object, e As EventArgs) Handles btnEliminarArtista.Click
        Dim ARux As Artista
        If txtIdArt.Text IsNot String.Empty And txtNombreArt.Text IsNot String.Empty And txtPaisArt.Text IsNot String.Empty And txtImagenArt.Text IsNot String.Empty Then
            ARux = New Artista(txtIdArt.Text, txtNombreArt.Text, txtPaisArt.Text, txtImagenArt.Text)
            If MessageBox.Show("¿Esta seguro de eliminar el Artista con id: " & ARux.idArtistaART & " ?", "Correcto", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                ARux = New Artista(txtIdArt.Text)
                ARux.nombreART = txtNombreArt.Text
                Try
                    If ARux.eliminarArtista() <> 1 Then
                        MessageBox.Show("Delete return distinto de 1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End If
                Catch ex As Exception
                    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End Try
                lstArtistas.Items.Remove(ARux.idArtistaART)
                btnLimpiarArtista.PerformClick()
                MessageBox.Show("Artista eliminado correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    Private Sub btnModificarArtista_Click(sender As Object, e As EventArgs) Handles btnModificarArtista.Click
        Dim ARux As Artista
        If txtIdArt.Text IsNot String.Empty And txtNombreArt.Text IsNot String.Empty And txtPaisArt.Text IsNot String.Empty And txtImagenArt.Text IsNot String.Empty Then
            ARux = New Artista(txtIdArt.Text, txtNombreArt.Text, txtPaisArt.Text, txtImagenArt.Text)
            Try
                If ARux.modificarArtista() <> 1 Then
                    MessageBox.Show("Update return distinto de 1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
            MessageBox.Show("Artista con el id: " & ARux.idArtistaART & " ha sido actualizado correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub btnFavorito_Click(sender As Object, e As EventArgs) Handles btnFavorito.Click
        Dim ARux As Artista
        If txtIdArt.Text IsNot String.Empty And txtNombreArt IsNot String.Empty And txtPaisArt.Text IsNot String.Empty And txtImagenArt.Text IsNot String.Empty Then
            ARux = New Artista(txtIdArt.Text, txtNombreArt.Text, txtPaisArt.Text, txtImagenArt.Text)
            If ARux.favorito(UsuarioS) Then
                MessageBox.Show("El artista ya esta añadido a favoritos ", "Añadido a favoritos", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                Try
                    ARux.insertarARTFav(UsuarioS)
                    lstArtistasFav.Items.Add(ARux.idArtistaART)
                    lblEsFavorito.Text = "Artista marcado como favorito"
                    lblEsFavorito.ForeColor = Color.Green
                Catch ex As Exception
                    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End Try
            End If
        End If
    End Sub

    Private Sub btnQuitarFav_Click(sender As Object, e As EventArgs) Handles btnQuitarFav.Click
        Dim ARux As Artista
        Dim idART As Integer
        If lstArtistasFav.SelectedItem() IsNot Nothing Then
            idART = lstArtistasFav.SelectedItem()
            ARux = New Artista(idART)
            If ARux.favorito(UsuarioS) Then
                Try
                    ARux.eliminarARTFav(UsuarioS)
                Catch ex As Exception
                    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End Try
            Else
                MessageBox.Show("El artista no esta añadido a favoritos ", "Sin añadir a favoritos", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
            lstArtistasFav.Items.Remove(ARux.idArtistaART)
        End If
    End Sub

    'METODOS DE LOS BOTONES DE CANCION'
    Private Sub btnAnadirCancion_Click(sender As Object, e As EventArgs) Handles btnAnadirCancion.Click
        Dim CAux As Cancion
        Dim ABux As Album = New Album
        Dim anadido As Boolean = True
        ABux.leerTodoAlbum(txtRuta.Text)
        If txtIdCanc.Text IsNot String.Empty And txtNombreCanc.Text IsNot String.Empty And txtDuracionCanc.Text IsNot String.Empty And txtAlbumCanc.Text IsNot String.Empty Then
            For Each Ab As Album In ABux.AlbDAO.Albumes
                If Ab.idALB = txtAlbumCanc.Text Then
                    CAux = New Cancion(txtIdCanc.Text, txtNombreCanc.Text, txtAlbumCanc.Text, txtDuracionCanc.Text)
                    Try
                        If CAux.insertarCancion() <> 1 Then
                            MessageBox.Show("Insert return distinto de 1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Exit Sub
                        End If
                    Catch ex As Exception
                        MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End Try
                    lstCanciones.Items.Add(CAux.idCANC)
                    MessageBox.Show("Cancion con el id: " & CAux.idCANC & " ha sido añadida correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    anadido = False
                End If
            Next
            If anadido Then
                MessageBox.Show("No es posible añadir la Cancion, no existe ningún Album con el id: '" & txtAlbumCanc.Text & "' ", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    Private Sub btnEliminarCancion_Click(sender As Object, e As EventArgs) Handles btnEliminarCancion.Click
        Dim CAux As Cancion
        If txtIdCanc.Text IsNot String.Empty And txtNombreCanc.Text IsNot String.Empty And txtDuracionCanc.Text IsNot String.Empty And txtAlbumCanc.Text IsNot String.Empty Then
            CAux = New Cancion(txtIdCanc.Text)

            If MessageBox.Show("¿Esta seguro de eliminar la cancion : " & CAux.idCANC & " ?", "Correcto", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                CAux = New Cancion(txtIdCanc.Text)
                Try
                    If CAux.eliminarCancion() <> 1 Then
                        MessageBox.Show("Delete return distinto de 1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End If
                Catch ex As Exception
                    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End Try
                lstCanciones.Items.Remove(CAux.idCANC)
                btnLimpiarCancion.PerformClick()
                MessageBox.Show("Canción eliminado correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    Private Sub btnLimpiarCancion_Click(sender As Object, e As EventArgs) Handles btnLimpiarCancion.Click
        txtIdCanc.Text = String.Empty
        txtNombreCanc.Text = String.Empty
        txtDuracionCanc.Text = String.Empty
        txtAlbumCanc.Text = String.Empty
        btnAnadirCancion.Enabled = True
        btnModificarCancion.Enabled = False
        btnEliminarCancion.Enabled = False
        txtDuracionCanc.ReadOnly = False
    End Sub

    Private Sub btnModificarCancion_Click(sender As Object, e As EventArgs) Handles btnModificarCancion.Click
        Dim CAux As Cancion
        If txtIdCanc.Text IsNot String.Empty And txtNombreCanc.Text IsNot String.Empty And txtAlbumCanc.Text IsNot String.Empty Then
            CAux = New Cancion(txtIdCanc.Text, txtNombreCanc.Text, txtAlbumCanc.Text)
            Try
                If CAux.modificarCancion() <> 1 Then
                    MessageBox.Show("Update return distinto de 1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
            MessageBox.Show("Cancion con el id: " & CAux.idCANC & " ha sido actualizado correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    'METODOS DE LOS BOTONES DE ALBUM'
    Private Sub btnAnadirAlbum_Click(sender As Object, e As EventArgs) Handles btnAnadirAlbum.Click
        Dim ABux As Album
        Dim ARux As Artista = New Artista
        Dim anadido As Boolean = True
        ARux.leerTodoArtista(txtRuta.Text)
        If txtIdAlbum.Text IsNot String.Empty And txtNombreAlbum.Text IsNot String.Empty And txtFechaAlbum.Text IsNot String.Empty And txtIdArtistaAlb.Text IsNot String.Empty And txtPortadaAlbum.Text IsNot String.Empty Then
            For Each Ar As Artista In ARux.ArDAO.Artistas
                If Ar.idArtistaART = txtIdArtistaAlb.Text Then
                    ABux = New Album(txtIdAlbum.Text, txtNombreAlbum.Text, txtFechaAlbum.Text, txtIdArtistaAlb.Text, txtPortadaAlbum.Text)
                    Try
                        If ABux.insertarAlbum() <> 1 Then
                            MessageBox.Show("Insert return distinto de 1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Exit Sub
                        End If
                    Catch ex As Exception
                        MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End Try
                    lstAlbumes.Items.Add(ABux.idALB)
                    calcularDuracionAlbum(ABux)
                    MessageBox.Show("Album con el id: " & ABux.idALB & " ha sido añadido correctamente", "Modificar Album", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    anadido = False
                End If
            Next
            If anadido Then
                MessageBox.Show("No es posible añadir el Album, no existe ningún Artista con el id: '" & txtIdArtistaAlb.Text & "' ", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    Private Sub btnLimpiarAlbum_Click(sender As Object, e As EventArgs) Handles btnLimpiarAlbum.Click
        txtIdAlbum.Text = String.Empty
        txtNombreAlbum.Text = String.Empty
        txtFechaAlbum.Text = String.Empty
        txtIdArtistaAlb.Text = String.Empty
        txtPortadaAlbum.Text = String.Empty
        txtDuracionAlbum.Text = String.Empty
        pbAlbumes.Image.Dispose()
        pbAlbumes.Image = Nothing
        btnAnadirAlbum.Enabled = True
        btnModificarAlbum.Enabled = False
        btnEliminarAlbum.Enabled = False
        lstCancionesAlbum.Items.Clear()

    End Sub

    Private Sub btnEliminarAlbum_Click(sender As Object, e As EventArgs) Handles btnEliminarAlbum.Click
        Dim ABux As Album
        If txtIdAlbum.Text IsNot String.Empty And txtNombreAlbum.Text IsNot String.Empty And txtFechaAlbum.Text IsNot String.Empty And txtIdArtistaAlb.Text IsNot String.Empty And txtPortadaAlbum.Text IsNot String.Empty Then
            ABux = New Album(txtIdAlbum.Text, txtNombreAlbum.Text, txtFechaAlbum.Text, txtIdArtistaAlb.Text, txtPortadaAlbum.Text)
            If MessageBox.Show("¿Esta seguro de eliminar el Album con id: " & ABux.idALB & " ?", "Eliminar Album", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                ABux = New Album(txtIdAlbum.Text)
                ABux.nombreALB = txtNombreAlbum.Text

                Try
                    If ABux.eliminarAlbum() <> 1 Then
                        MessageBox.Show("Delete return distinto de 1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End If
                Catch ex As Exception
                    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End Try
                lstAlbumes.Items.Remove(ABux.idALB)
                btnLimpiarAlbum.PerformClick()
                MessageBox.Show("Album eliminado correctamente", "Modificar Album", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    Private Sub btnModificarAlbum_Click(sender As Object, e As EventArgs) Handles btnModificarAlbum.Click
        Dim ABux As Album
        If txtIdAlbum.Text IsNot String.Empty And txtNombreAlbum.Text IsNot String.Empty And txtFechaAlbum.Text IsNot String.Empty And txtIdArtistaAlb.Text IsNot String.Empty And txtPortadaAlbum.Text IsNot String.Empty Then
            ABux = New Album(txtIdAlbum.Text, txtNombreAlbum.Text, txtFechaAlbum.Text, txtIdArtistaAlb.Text, txtPortadaAlbum.Text)
            Try
                If ABux.modificarAlbum() <> 1 Then
                    MessageBox.Show("Update return distinto de 1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
            MessageBox.Show("Album con el id: " & ABux.idALB & " ha sido actualizado correctamente", "Modificar Album", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    'METODOS DE LOS BOTONES DE REPRODUCCION ***********************************************************************************************************************************' 
    Private Sub btnLimpiarR_Click(sender As Object, e As EventArgs) Handles btnLimpiarR.Click
        txtIdReproduccion.Text = String.Empty
        txtCancionReproduccion.Text = String.Empty
        txtFechaReproduccion.Text = String.Empty
        lblReproduciendo.Text = ""
        btnAnadirR.Enabled = True
    End Sub

    Private Sub btnAnadirR_Click(sender As Object, e As EventArgs) Handles btnAnadirR.Click
        Dim RAux As Reproduccion
        Dim fechaRepro As Date

        fechaRepro = Today

        If txtUsuarioReproduccion.Text IsNot String.Empty And txtIdReproduccion.Text IsNot String.Empty And txtCancionReproduccion.Text IsNot String.Empty Then
            RAux = New Reproduccion(txtIdReproduccion.Text, txtUsuarioReproduccion.Text, txtCancionReproduccion.Text, fechaRepro)
            Try
                If RAux.insertarReproduccion() <> 1 Then
                    MessageBox.Show("Insert return distinto de 1", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
            lstReproducciones.Items.Add(RAux.IdReproduccion)
            lblReproduciendo.Text = "Reproduciendo"
            lblReproduciendo.ForeColor = Color.Green
            MessageBox.Show("Reproduccion con el id: " & RAux.IdReproduccion & " ha sido añadida correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    '******************************************************************************************************************************************************************************************'

    'CONSULTAS'

    '1: Listado de artistas ordenados por número de reproducciones (en general y permitiendo filtrar por país).'
    Private Sub btnARTMasEscuchados_Click(sender As Object, e As EventArgs) Handles btnOrdenarCAN.Click
        'POR ACABAR'
    End Sub

    '2: Listado de canciones ordenadas por número de reproducciones.'
    Private Sub btnOrdenarCAN_Click(sender As Object, e As EventArgs) Handles btnOrdenarCAN.Click
        Dim CanDAOux As CancionDAO = New CancionDAO
        Dim CAord As Collection
        lstCanOrd.Items.Clear()

        Try
            CanDAOux.cancionesXRepro()
            CAord = CanDAOux.CancionesPorRepro()
            If CAord.Count > 0 Then
                For i As Integer = 1 To CAord.Count
                    lstCanOrd.Items.Add(CAord.Item(i))
                Next
            End If

        Catch ex As Exception
            MessageBox.Show("No hay canciones que ordenar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try

    End Sub

    '3: Listado de artistas más escuchados por un usuario (entre 2 fechas).'
    Private Sub btnARTMasEscuchadoUS_Click(sender As Object, e As EventArgs) Handles btnARTMasEscuchadoUS.Click
        Dim UsDAOux As UsuarioDAO = New UsuarioDAO
        Dim ArtMasEscUs As Collection
        lstArtMEscuchadosUS.Items.Clear()

        If lstUsuarios.SelectedItem IsNot Nothing Then
            Try
                UsDAOux.artistasEscuchados(dtpDesde.Value.ToShortDateString, dtpHasta.Value.ToShortDateString, UsuarioS)
                ArtMasEscUs = UsDAOux.ArtistasMEscuchados()
                '''''''''
                If ArtMasEscUs.Count > 0 Then
                    For i As Integer = 1 To ArtMasEscUs.Count
                        lstArtMEscuchadosUS.Items.Add(ArtMasEscUs.Item(i))
                    Next
                End If

            Catch ex As Exception
                MessageBox.Show("No hay artistas que mostrar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
        End If
    End Sub

    '4. Listado de usuarios ordenado por el tiempo que utilizan la aplicación (en base a la duración total de las canciones reproducidas)'
    Private Sub btnMostrarUsMasT_Click(sender As Object, e As EventArgs) Handles btnMostrarUsMasT.Click
        Dim UsDAOux As UsuarioDAO = New UsuarioDAO
        Dim UsMasT As Collection
        lstUsMasT.Items.Clear()

        Try
            UsDAOux.usuarioMasT()
            UsMasT = UsDAOux.UsuariosMasT()

            If UsMasT.Count > 0 Then
                For i As Integer = 1 To UsMasT.Count
                    lstUsMasT.Items.Add(UsMasT.Item(i))
                Next
            End If

        Catch ex As Exception
            MessageBox.Show("No hay usuarios que mostrar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
    End Sub

    '5: Tiempo de reproducción de los artistas favoritos de un usuario.'
    Private Sub btnTReproArtFav_Click(sender As Object, e As EventArgs) Handles btnTReproArtFav.Click
        Dim UsDAOux As UsuarioDAO = New UsuarioDAO
        Dim TRepro As Collection
        lstTReproArtFav.Items.Clear()

        If lstUsuarios.SelectedItem IsNot Nothing Then
            Try
                UsDAOux.tReproArtFav(UsuarioS)

                TRepro = UsDAOux.TReproTotalArtFav()

                If TRepro.Count > 0 Then
                    For i As Integer = 1 To TRepro.Count
                        lstTReproArtFav.Items.Add(TRepro.Item(i))
                    Next
                End If

            Catch ex As Exception
                MessageBox.Show("No hay reproducciones de los artistas favoritos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End Try
        End If
    End Sub


End Class