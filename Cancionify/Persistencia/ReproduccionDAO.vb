﻿Public Class ReproduccionDAO

    Public ReadOnly Property Reproducciones As Collection

    Public Sub New()
        Me.Reproducciones = New Collection
    End Sub

    Public Sub leerTodo(u As Usuario)
        Dim r As Reproduccion
        Dim col, aux As Collection
        col = AgenteBD.ObtenerAgente.Leer("SELECT * FROM Reproducciones WHERE Usuario='" & u.emailUS & "' ORDER BY idReproduccion")
        For Each aux In col
            r = New Reproduccion(aux(1).ToString, aux(2).ToString, aux(3).ToString, aux(4).ToString)
            Me.Reproducciones.Add(r)
        Next
    End Sub

    Public Sub leer(ByRef r As Reproduccion)
        Dim col As Collection : Dim aux As Collection
        col = AgenteBD.ObtenerAgente.Leer("SELECT * FROM Reproducciones WHERE idReproduccion='" & r.IdReproduccion & "';")
        For Each aux In col
            r.usuarioRepro = New Usuario(aux(2).ToString)
            r.cancionRepro = New Cancion(Integer.Parse(aux(3).ToString))
            r.fechaRepro = aux(4).ToString
        Next
    End Sub

    Public Function insertar(ByVal r As Reproduccion) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("INSERT INTO Reproducciones VALUES (" & r.IdReproduccion & ", '" & r.usuarioRepro.emailUS & "', " & r.cancionRepro.idCANC & ", '" & r.fechaRepro & "');")
    End Function

    Public Function modificar(ByVal r As Reproduccion) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("UPDATE Reproducciones SET Cancion=" & r.cancionRepro.idCANC & ", Fecha='" & r.fechaRepro & "' WHERE idReproduccion=" & r.IdReproduccion & ";")
    End Function

    Public Function eliminar(ByVal r As Reproduccion) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("DELETE FROM Reproducciones WHERE idReproduccion='" & r.IdReproduccion & "';")
    End Function

End Class
