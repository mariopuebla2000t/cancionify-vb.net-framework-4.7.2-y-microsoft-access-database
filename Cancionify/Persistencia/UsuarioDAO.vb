﻿Public Class UsuarioDAO
    Public ReadOnly Property Usuarios As Collection
    Public ReadOnly Property ReproduccionesUsuarios As Collection
    Public ReadOnly Property ArtistasFavoritosUsuarios As Collection
    Public ReadOnly Property ArtistasMEscuchados As Collection
    Public ReadOnly Property UsuariosMasT As Collection
    Public ReadOnly Property TReproTotalArtFav As Collection


    Public Sub New()
        Me.Usuarios = New Collection
        Me.ReproduccionesUsuarios = New Collection
        Me.ArtistasFavoritosUsuarios = New Collection
        Me.ArtistasMEscuchados = New Collection
        Me.UsuariosMasT = New Collection
        Me.TReproTotalArtFav = New Collection
    End Sub

    Public Sub leerTodo(ruta As String)
        Dim u As Usuario
        Dim col, aux As Collection
        col = AgenteBD.ObtenerAgente(ruta).Leer("SELECT * FROM Usuarios ORDER BY Email")
        For Each aux In col
            u = New Usuario(aux(1).ToString, aux(2).ToString, aux(3).ToString, aux(4).ToString)
            Me.Usuarios.Add(u)
        Next
    End Sub

    Public Sub leer(ByRef u As Usuario)
        Dim col As Collection : Dim aux As Collection
        col = AgenteBD.ObtenerAgente.Leer("SELECT * FROM Usuarios WHERE email='" & u.emailUS & "';")
        For Each aux In col
            u.nombreUS = aux(2).ToString
            u.apellidoUS = aux(3).ToString
            u.fechaNacimientoUS = aux(4).ToString
        Next
    End Sub

    Public Function insertar(ByVal u As Usuario) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("INSERT INTO Usuarios VALUES ('" & u.emailUS & "', '" & u.nombreUS & "','" & u.apellidoUS & "','" & u.fechaNacimientoUS & "')")
    End Function

    Public Function eliminar(ByVal u As Usuario) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("DELETE FROM Usuarios WHERE email='" & u.emailUS & "';")
    End Function

    Public Function modificar(ByVal u As Usuario) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("UPDATE Usuarios SET nombre='" & u.nombreUS & "', apellidos='" & u.apellidoUS & "',fechaNacimiento='" & u.fechaNacimientoUS & "'WHERE email='" & u.emailUS & "';")
    End Function

    'Consulta numero 3 : Artistas más escuchados por un usuario'
    Public Sub artistasEscuchados(ByVal fInicio As Date, ByVal fFin As Date, ByVal u As Usuario)
        Dim col As Collection : Dim aux As Collection
        col = AgenteBD.ObtenerAgente.Leer("SELECT a.nombre FROM ARTISTAS a, REPRODUCCIONES r, CANCIONES c, ALBUMES al WHERE r.fecha BETWEEN #" & fInicio & "# AND #" & fFin & "# AND r.cancion = c.IdCancion AND c.album = al.idalbum AND al.artista = a.idartista AND Usuario='" & u.emailUS & "' GROUP BY a.nombre ORDER BY COUNT(a.idartista) DESC;")
        For Each aux In col
            ArtistasMEscuchados.Add(aux(1).ToString)
        Next
    End Sub

    'Consulta numero 4 : Usuarios mas activos'
    Public Sub usuarioMasT()
        Dim col As Collection : Dim aux As Collection
        Dim ret As Collection = New Collection
        col = AgenteBD.ObtenerAgente.Leer("SELECT R.usuario, SUM(C.duracion) FROM REPRODUCCIONES R, CANCIONES C WHERE R.cancion = C.idcancion GROUP BY R.usuario ORDER BY SUM(C.duracion) DESC;")

        For Each aux In col
            Dim texto As String = aux(1).ToString & ": " & aux(2).ToString & " segundos"
            Me.UsuariosMasT.Add(texto)

        Next
    End Sub

    'Consulta numero 5 : Tiempo reproduccion artistas favoritos del usuario'
    Public Sub tReproArtFav(u As Usuario)
        Dim col As Collection : Dim aux As Collection
        col = AgenteBD.ObtenerAgente.Leer("SELECT a.nombre, SUM(c.duracion) FROM ARTISTAS a, CANCIONES c, ALBUMES al, REPRODUCCIONES r WHERE a.idartista IN (SELECT Artista FROM ARTISTAS_FAVORITOS WHERE Usuario= '" & u.emailUS & "') AND r.usuario= '" & u.emailUS & "'AND c.idcancion = r.cancion AND c.album = al.idalbum AND al.artista = a.idartista GROUP BY a.nombre;")
        For Each aux In col
            Me.TReproTotalArtFav.Add(aux(1).ToString & ": " & aux(2).ToString & " segundos")
        Next
    End Sub

End Class
