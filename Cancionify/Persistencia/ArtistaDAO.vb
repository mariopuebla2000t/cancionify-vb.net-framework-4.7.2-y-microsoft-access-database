﻿Public Class ArtistaDAO
    Public ReadOnly Property Artistas As Collection

    Public Sub New()
        Me.Artistas = New Collection
    End Sub

    Public Sub leerTodo(ruta As String)
        Dim ar As Artista
        Dim col, aux As Collection
        col = AgenteBD.ObtenerAgente(ruta).Leer("SELECT * FROM Artistas ORDER BY idArtista")
        For Each aux In col
            ar = New Artista(Convert.ToInt32(aux(1)), aux(2).ToString, aux(3).ToString, aux(4).ToString)
            Me.Artistas.Add(ar)
        Next
    End Sub

    Public Sub leer(ByRef ar As Artista)
        Dim col As Collection : Dim aux As Collection
        col = AgenteBD.ObtenerAgente.Leer("SELECT * FROM Artistas WHERE IdArtista=" & ar.idArtistaART & ";")
        For Each aux In col
            ar.nombreART = aux(2).ToString
            ar.paisART = aux(3).ToString
            ar.imagenART = aux(4).ToString
        Next
    End Sub

    Public Function insertar(ByVal ar As Artista) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("INSERT INTO Artistas VALUES (" & ar.idArtistaART & ", '" & ar.nombreART & "', '" & ar.paisART & "', '" & ar.imagenART & "');")
    End Function

    Public Function modificar(ByVal ar As Artista) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("UPDATE Artistas SET nombre='" & ar.nombreART & "', pais='" & ar.paisART & "',imagen='" & ar.imagenART & "'WHERE idArtista=" & ar.idArtistaART & ";")
    End Function

    Public Function eliminar(ByVal ar As Artista) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("DELETE FROM Artistas WHERE idArtista=" & ar.idArtistaART & ";")
    End Function

    'CONSULTAS DE ARTISTAS FAVORITOS
    Public Function leerFavs(ByRef a As Artista, ByRef u As Usuario) As Boolean
        Dim col As Collection
        col = AgenteBD.ObtenerAgente.Leer("SELECT * FROM ARTISTAS_FAVORITOS WHERE Usuario='" & u.emailUS & "' AND Artista=" & a.idArtistaART & ";")

        If col.Count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function insertarFav(ByRef a As Artista, ByRef u As Usuario) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("INSERT INTO Artistas_Favoritos VALUES ('" & u.emailUS & "'," & a.idArtistaART & ",'" & DateTime.Today & "');")
    End Function

    Public Function eliminarFav(ByRef a As Artista, ByRef u As Usuario) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("DELETE FROM Artistas_Favoritos WHERE Usuario='" & u.emailUS & "' AND Artista= " & a.idArtistaART & ";")
    End Function

    'Solicitar al dao de artista borrar todos los favoritos donde aparezca un usuario

    Public Function eliminarFavsDelUsuario(ByRef u As Usuario) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("DELETE FROM Artistas_Favoritos WHERE Usuario='" & u.emailUS & "';")
    End Function

    'Solicitar al dao de artista borrar todos los favoritos donde aparezca un artista

    Public Function eliminarArtistaDeFavs(ByRef a As Artista) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("DELETE FROM Artistas_Favoritos WHERE Artista=" & a.idArtistaART & ";")
    End Function

End Class
