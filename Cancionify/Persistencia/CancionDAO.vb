﻿Public Class CancionDAO

    Public ReadOnly Property Canciones As Collection
    Public ReadOnly Property CancionesPorRepro As Collection

    Public Sub New()
        Me.Canciones = New Collection
        Me.CancionesPorRepro = New Collection
    End Sub

    Public Sub leerTodo(ruta As String)
        Dim c As Cancion
        Dim col, aux As Collection
        col = AgenteBD.ObtenerAgente(ruta).Leer("SELECT * FROM Canciones ORDER BY idCancion")
        For Each aux In col
            c = New Cancion(Convert.ToInt32(aux(1)), aux(2).ToString, Convert.ToInt32(aux(3)), Convert.ToInt32(aux(4)))
            Me.Canciones.Add(c)
        Next
    End Sub

    Public Sub leer(ByRef c As Cancion)
        Dim col As Collection : Dim aux As Collection
        col = AgenteBD.ObtenerAgente.Leer("SELECT * FROM Canciones WHERE idCancion=" & c.idCANC & ";")
        For Each aux In col
            c.nombreCANC = aux(2).ToString
            c.albumCANC = Convert.ToInt32(aux(3))
            c.duracionCANC = Convert.ToInt32(aux(4))
        Next
    End Sub

    Public Function insertar(ByVal c As Cancion) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("INSERT INTO Canciones VALUES (" & c.idCANC & ", '" & c.nombreCANC & "', " & c.albumCANC & ", " & c.duracionCANC & ");")
    End Function

    Public Function modificar(ByVal c As Cancion) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("UPDATE Canciones SET nombre='" & c.nombreCANC & "', album=" & c.albumCANC & " WHERE idCancion=" & c.idCANC & ";")
    End Function

    Public Function eliminar(ByVal c As Cancion) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("DELETE FROM Canciones WHERE idCancion=" & c.idCANC & ";")
    End Function

    'Consulta numero 2 : Listado de canciones ordenadas por número de reproducciones'
    Public Sub cancionesXRepro()
        Dim col As Collection : Dim aux As Collection
        col = AgenteBD.ObtenerAgente.Leer("SELECT c.nombre , COUNT(r.cancion) FROM CANCIONES c, REPRODUCCIONES r WHERE c.idcancion = r.cancion GROUP BY c.nombre ORDER BY COUNT(r.cancion) DESC;")

        For Each aux In col
            CancionesPorRepro.Add(aux(1).ToString + ": " + aux(2).ToString + " reproducciones")
        Next
    End Sub

End Class
