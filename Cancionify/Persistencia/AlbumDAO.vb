﻿Public Class AlbumDAO

    Public ReadOnly Property Albumes As Collection
    Public ReadOnly Property AlbumesArtistas As Collection
    Public Sub New()
        Me.Albumes = New Collection
    End Sub

    Public Sub leerTodo(ruta As String)
        Dim al As Album
        Dim col, aux As Collection
        col = AgenteBD.ObtenerAgente(ruta).Leer("SELECT * FROM Albumes ORDER BY idAlbum")
        For Each aux In col
            al = New Album(aux(1).ToString, aux(2).ToString, aux(3).ToString, aux(4).ToString, aux(5).ToString)
            Me.Albumes.Add(al)
        Next
    End Sub

    Public Sub leer(ByRef al As Album)
        Dim col As Collection : Dim aux As Collection
        col = AgenteBD.ObtenerAgente.Leer("SELECT * FROM Albumes WHERE idAlbum=" & al.idALB & ";")
        For Each aux In col
            al.nombreALB = aux(2).ToString
            al.fechaALB = aux(3).ToString
            al.idArtistaALB = aux(4).ToString
            al.portadaALB = aux(5).ToString
        Next
    End Sub

    Public Function insertar(ByVal al As Album) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("INSERT INTO Albumes VALUES (" & al.idALB & ", '" & al.nombreALB & "', '" & al.fechaALB & "', " & al.idArtistaALB & ", '" & al.portadaALB & "');")
    End Function

    Public Function modificar(ByVal al As Album) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("UPDATE Albumes SET nombre='" & al.nombreALB & "' ,fecha='" & al.fechaALB & "' ,portada='" & al.portadaALB & "' WHERE idAlbum=" & al.idALB & ";")
    End Function

    Public Function eliminar(ByVal al As Album) As Integer
        Return AgenteBD.ObtenerAgente.Modificar("DELETE FROM Albumes WHERE idAlbum=" & al.idALB & ";")
    End Function

End Class
