# Cancionify - VB.NET Framework 4.7.2 y Microsoft Access Database
Proyecto desarrollado entre 2 alumnos como parte de la asignatura **[42319 - Bases de datos](https://guiae.uclm.es/vistaGuia/407/42319)** en el segundo año de carrera universitaria. En este trabajo, se ha creado una aplicación que simula un sistema parecido a _Spotify_, con acceso a una base de datos Microsoft Access utilizando VB.NET Framework 4.7.2 y SQL. 

## Tecnologías Utilizadas
**Lenguaje de programación**: VB.NET y SQL  
**Herramientas de desarrollo**: Visual Studio 2019 y Microsoft Access  

Más información en los PDF's asociados.
